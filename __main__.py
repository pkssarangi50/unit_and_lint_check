from pathlib import Path
import threading

from apps.heartbeat.heartbeat import Heartbeat
from common.utils.network_location_handler import dispose_network_location
from common.connectors_executor.connectors_executor import ConnectorsExecutor
from common.utils.config_helper import Config, ConfigLoader
from common.utils import xlogging
from common.utils.network_location_handler import mount_network_location
from common.mqtt.mac_mqtt import MacMqttConfiguration, MacMqtt

# initializing Logger
LOGGER = xlogging.getLogger(__name__)

def configure_and_init_mqtt(configuration: Config) -> MacMqtt:
    '''Create an instance of MacMqttConfiguration using the config'''
    LOGGER.info('Mqtt Config creation started')
    mqtt_config = MacMqttConfiguration(
            configuration.ie_databus_host,
            int(configuration.ie_databus_port),
            configuration.ie_databus_username,
            configuration.ie_databus_password,
            'machine-connector',
            'machine-connector'
        )
    LOGGER.info('Mqtt Config creation completed')
    # Create an instance of MacMqtt using the mqtt_config
    mqtt = MacMqtt(configuration=mqtt_config,
                   logger_basename= LOGGER._logger.name)
    return mqtt


try:
    config_file_path = (Path(__file__).resolve().parents[0]/
                            "config"/ "machine-connector-config.json")
    
    LOGGER.info('loading configuration started')
    loader = ConfigLoader(config_file_path)
    loader.load_config()
    LOGGER.info('loading configuration completed')

    LOGGER.info('Config object creation started')
    config_object = loader.convert_to_config_object()
    LOGGER.info('Config object creation completed')

    if config_object:
        #clear existing cache objects
        dispose_network_location(config_object.reader.storage_base_path)
        if config_object.writer:
            dispose_network_location(config_object.writer.storage_base_path)

        #MQTT #init MQTT & connect
        LOGGER.info('Mqtt initialization started')
        mqtt_client: MacMqtt = configure_and_init_mqtt(config_object)
        LOGGER.info('Mqtt initialization completed')

        isInitializationFailed:bool = False
        if mqtt_client.is_connected():
            LOGGER.info('MQTT connection successful')
        else:
            LOGGER.error("MQTT connection failed.")
            isInitializationFailed = True
        
        if (config_object.reader or config_object.writer):
            LOGGER.info('Configuration initialization successful')
        else:
            LOGGER.error("Configuration initialization failed, atleast Reader or Writer should be configured")
            isInitializationFailed = True
        
        if config_object.reader and mount_network_location(config_object.reader.storage_base_path, config_object.reader.storage_mount_username, config_object.reader.storage_mount_password, config_object.reader.storage_is_mount):
            LOGGER.info('Reader base path available')
        else:
            LOGGER.error("Reader base path invalid")
            isInitializationFailed = True

        if config_object.writer and mount_network_location(config_object.writer.storage_base_path, config_object.writer.storage_mount_username, config_object.writer.storage_mount_password, config_object.writer.storage_is_mount):
            LOGGER.info('Writer base path available')
        # else:
        #     LOGGER.info('Writer base path invalid')
        #     isInitializationFailed = True

        if not isInitializationFailed:
            LOGGER.info("Starting Machine Observer.")

            # connectors
            connectors_executor = ConnectorsExecutor(config_object, mqtt_client)
            threads_to_join = []

            # creating threads for Reader
            if config_object.reader and len(config_object.reader.endpoints) > 0:
                reader_thread = threading.Thread(target=connectors_executor.fileReader_executor, name="reader_main_thread")
               
                # starting threads for Reader
                reader_thread.start()
                threads_to_join.append(reader_thread)
            
             # creating threads for Writer
            if config_object.writer and config_object.writer.endpoints:
                writer_thread = threading.Thread(target=connectors_executor.fileWriter_executor, name="writer_main_thread")

                # starting threads for Writer
                writer_thread.start()
                threads_to_join.append(writer_thread)

            LOGGER.info("Starting Machine Observer completed.")

            for _thread in threads_to_join:
                _thread.join()

    else:
        LOGGER.error("Invalid config file.")
    
    #clear existing cache objects
    dispose_network_location(config_object.reader.storage_base_path)
    if config_object.writer:
        dispose_network_location(config_object.writer.storage_base_path)
except Exception as exp:
    LOGGER.error(f"Application cannot be started as {exp}")