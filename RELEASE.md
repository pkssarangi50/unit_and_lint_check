# Project Releases

[[_TOC_]]

## Overall

For each release the changes are grouped according to their impact on the project:

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for once-stable features removed in upcoming releases.
- `Removed` for (deprecated) features removed in this release.
- `Fixed` for any bug fixes.
- `Security` to invite users to upgrade in case of vulnerabilities.

## Last changes

- Added
  - Install, Upgrade and Removal of Machine Connector application. ([MR-3](!3), [MR-61](!61), [MR-15](!15))
  - Observer for network share of AOI raw input files. ([MR-3](!3))
  - Sending compressed payload to the edge device via MQTT. ([MR-3](!3))
  - Heartbeat feature to enable ping mechanism for the application. ([MR-3](!3))
  - Deployable package creation for windows using automated pipelines. ([MR-3](!3), [MR-36](!36), [MR-73](!73))
  - Establishing CICD with standard automated test and release stages. ([MR-3](!3))
