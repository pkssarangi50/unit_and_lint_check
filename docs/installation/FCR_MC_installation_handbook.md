[[_TOC_]]

# **Preface**
Additional Information
If you are using a non-English OS, please send an e-mail to l advdaadassdedsm-falcon.in@internal.siemens.com for support.


# **Introduction**

## **Prerequisites**

### **System Requirement**

The following are the prerequisites to run Machine Connector optimally. 
1. A trusted network zone (for example, Restricted VLAN ) should be installed to ensure secure data transmission. 
2. Machine Connector: This service should be part of the secure network zone mentioned above. 
3. Windows 10 or Windows server 2019



# **Description**

## **Configuring Machine Connector**

1.	**Updating Installation Configurations**

Machine connector installation configuration can be found under the following path
<ExtractedZip>/config/ machine-connector-installconfig.json . 
Here you can find the template configuration. Machine connector configuration details must be updated as per the client machine specifications and paths should be specified accordingly based on the local machine paths. For any details of the attribute in the configuration please look at the Annexure [4]


2. **Updating Machine connector Configurations**
   
Machine connector configuration can be found under the following path
<ExtractedZip>/config/ machine-connector-config.json . 
Here you can find the template configuration. Machine connector configuration details must be updated as per the client machine specifications and paths should be specified accordingly based on the local machine paths. For any details of the attribute in the configuration please look at the Annexure [4] .


3.	**Mandatory Configurations**

User should update the following attributes of the configuration which are mandatory to have machine connector up and running.
IE_DATABUS_HOST, IE_DATABUS_PORT, IE_DATABUS_USERNAME, IE_DATABUS_PASSWORD, reader_path, success_path, error_conversion_path,
error_connection_path,STORAGE_IS_MOUNT, STORAGE_MOUNT_USERNAME, STORAGE_MOUNT_PASSWORD. For any further more detail information regarding the attributes please refer to the Annexure [4].


## **Installation Machine connector in Windows**
The zip folder which has been received should be unzipped, and after the configuration update, we should launch the launch_external.bat batch file, which indeed need to be executed as an administrator. 

![Launching Batch Script](installation_handbook_images/InstallationWindowsSelectLaunch.png)

### **Fresh Install**

With the launch of batch file user gets the prompt to specify the `Installation Path`:

![Batch Script Initialized Screen](installation_handbook_images/InitialPromptForInstallPath.png)

The user gets options to select as shown below, with basic explanation for each option. Presently the mode will be `install`:
 
![Initialized Screen](installation_handbook_images/launchbatInitializationScreen_fresh_install.png)

With [configuration](#configuring-machine-connector) on place, user can directly type “imc” Install Machine Connector Service, which will run the installation script and machine connector service and its dependencies will be installed. 

![Selection of Install Machine Connector Option](installation_handbook_images/launchbatIMCSelectionScreen.png)
 
When the installation is successful, we can see the console output as follows which specifies the Machine Connector Service installation is Completed

![IMC Successful](installation_handbook_images/launchbatIMCInstallationSucessfulScreen.png)
 
Installed Machine Connector service can be seen under the system services.
Click on  and type “Services”, under which we can see the FCR-Connector-Service up and running.

![FCR-MC-Service-Status](installation_handbook_images/FCRServiceStatusScreen.png)

Once the machine connector service is installed then the mode changes from `install` to `upgrade`. You can verify the same by selecting the `help(h)` option.

![FCR-installer-mdoe](installation_handbook_images/launchbatModePostFreshInstall.png)

**Note:** In the above screenshot, user can observe one message, `Upgrade Machine Connector is available from the extracted source location`, which suggest that user can initiate upgrade via `launch_external.bat` from a location where he/she has the source extracted and not from this terminal that is open. He has the option to remove the machine connector service using the `rmc` option from the same terminal.

**Removing Machine connector service from Windows**
![Removing Machine Connector Service](installation_handbook_images/RMCRemovingMCService.png)

**Removing Machine connector service from Console**
![Validating in Services.msc](installation_handbook_images/RemovedMCServiceConsole.png)

**Neccessary folder structure has removed except Logs**
![Validating in Services.msc](installation_handbook_images/AllFolderRemovedExceptLogs.png)

## **Upgrade Machine connector in Windows**

After downloading the updated zip and extracting it in any desired location, user should launch the `launch_external.bat` batch file, which indeed need to be executed as an administrator. 

![Launching Batch Script](installation_handbook_images/InstallationWindowsSelectLaunch.png)
 
With the launch of batch file user gets options to select as shown below, with basic explanation for each option:

![Batch Script Initialized Screen](installation_handbook_images/launchbatModePostFreshInstall.png)

User can directly type “umc” to upgrade Machine Connector Service, which will run the Upgrade script. 

![Upgrade Machine Connector Selection](installation_handbook_images/launchbatUMCSelectionScreen.png)
 
After entering the command “umc”, user may get three types of ugrade scenarios as explained below

### **No Upgrade** 
When the python and machine connector versions are same then, we will get message "python and machine connector of same version already installed"

![No Upgrade Needed](installation_handbook_images/No%20upgrade%20same%20version.png)

### **Partial Upgrade**: 
When any of the Machine connector package version, or python associated in a installation package is more than installed. then it is a partial upgrade, only associated package will be upgraded, not the entire application.

![Machine Connector Partially Upgrade](installation_handbook_images/MachineConnectorPartiallyUpgrading.png)

### **Complete Upgrade**
When Machine connector package version is more than installed, and python version of package is more than the installed . then it is a complete upgrade, only machine connector package will be upgraded, not dependent python and packages.

![Machine Connector Complete Upgrade](installation_handbook_images/MachineConnectorCompleteUpgrading.png)


## **Logging**

User can find the relevant logs of the installation process or the service related logs under the installation directory which he/she specified during fresh install.
The installation log file would be like `<installation directory>\logs\launch_YY-MM-DD.log`
The service log would be like `<installation directory>\logs\machine-connector`
  
# **Annexure**

IE_DATABUS_HOST: Host Address for the Mqtt Broker

IE_DATABUS_PORT: Mqtt Broker running port

IE_DATABUS_USERNAME: Username for the Mqtt

IE_DATABUS_PASSWORD: Password for the Mqtt

STARTUP_WAIT_TIME_IN_SEC: Initial wait timer to ensure mqtt is connected

CYCLE_TIME_IN_SEC: Cycle time in seconds

COMPRESSION: Whether the Compress incoming content in deflate

MAX_FILE_SIZE_LIMIT: Allowed payload size limit for the Mqtt bus

ENDPOINTS: Information about each endpoint

active: Defines whether an endpoint is active or not

file_extension: end point allowed extension

machine_station_name: active endpoint station name

machine_vendor_type: endpoint vendor type

machine_type: which describes machine types example will be AOI

file_content_time_zone: machine connector hosted system run time

interface_version: defines the version of the machine connector

out_topic: topic name into which mqtt bus payload must be published

compression: defines whether the compression exists or not

reader_path: folder which need to be observed for the AOI input file

success_path: folder to which if AOI input file must be sent if the conversion is successful

error_conversion_path: folder to which if AOI input file must be sent if the conversion is failed because of an error

error_connection_path: folder to which if AOI input file must be sent if the conversion is failed because of a connection

STORAGE_MOUNT_USERNAME: username for the mounted reader path storage.

STORAGE_MOUNT_PASSWORD: password for the mounted reader path storage.

STORAGE_IS_MOUNT: true: if the reader path is a mount network drive, which is secured and having username and password. false: if it reader path is not a network drive. 

**Errorcodes**:

1: Successfully Connected to Databus and file processed

2: Network share error (Invalid Credentials, Inaccesable network share, Permission Denial)

3: File processing Error

4: Generic Application Exception

**Note**: if it is a network drive then path should be specified with additional escape sequences. 
Example: \\\\inblr.ad001.abc.net\\DigitalEnterprise\\input\\koh_young

