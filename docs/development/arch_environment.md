
# **Prerequisites**
1. Install VMWare Player from software center
1. Download the VMDK files from https://s3.console.aws.amazon.com/s3/buckets/130913303844-adminshare?region=eu-central-1&prefix=Developer_Linux/VMware/23_11_2022/&showversions=false
1. Get VM Image files - https://aws.siemens.cloud/s3://cs-mac-adminshare/Developer_Linux/VMware/
1. Install Git from Software center and download code in Windows Machine folder("FCR_DATA" - name should be same) as below,
   1. cd FCR_DATA
   2. git clone git@code.siemens.com:MAC_Lab/implementation/fcr/code-fcr.git
   3. git clone --branch feature-file-connector git@code.siemens.com:MAC_Lab/implementation/fcr/machine-connectors.git

# **Steps**
1. Open VMware Player
1. Click on "Open Virtual Machine" link -> select .ovf file and provide name & new directory path
1. Edit Virtual machine settings - RAM - 16GB, Cores - 4 etc
1. Update Options -> Shared Folders -> delete existing dummy VM_Share folder by clicking on Remove button
1. Update Options -> Shared Folders -> add a shared folder - > Host Path - Browse a new empty directory("FCR_DATA"), Name- can be of any name , Folder should be of same name "FCR_DATA"
1. Open command prompt
1. setxkbmap -layout us
1. In VMWare machine, enable View hidden folder to see the below folder got to path ~/.ssh/code.siemens.com and add the private key of code.siemens.com
1. yay pyenv-virtualenv
   1. type 1
1. pyenv install 3.8.12
   1. . /home/vm-user/.pyenv/versions/3.8.12/bin/python3.8 -m venv /home/vm-user/.virtualenvs/simaticai_3_8_12
   2. . /home/vm-user/.virtualenvs/simaticai_3_8_12/bin/activate
1. cd /mnt/hgfs/FCR_DATA/code-fcr/src/components/
1. pip install wheel
1. pip install GitPython==3.1.27
1. pip install simaticai-1.2.0-py3-none-any.whl
1. git config --global --add safe.directory /mnt/hgfs/FCR_DATA/code-fcr
1. git config --global --add safe.directory /mnt/hgfs/FCR_DATA/machine-connectors
# **Creating input fodlers**
1. mkdir -p /mnt/hgfs/FCR_DATA/input/koh_young/error_connection
1. mkdir -p /mnt/hgfs/FCR_DATA/input/koh_young/error_conversion
1. mkdir -p /mnt/hgfs/FCR_DATA/input/koh_young/success
1. mkdir -p /mnt/hgfs/FCR_DATA/input/viscom_si/error_connection
1. mkdir -p /mnt/hgfs/FCR_DATA/input/viscom_si/error_conversion
1. mkdir -p /mnt/hgfs/FCR_DATA/input/viscom_si/success
1. mkdir -p /mnt/hgfs/FCR_DATA/input/viscom_vvision/error_connection
1. mkdir -p /mnt/hgfs/FCR_DATA/input/viscom_vvision/error_conversion
1. mkdir -p /mnt/hgfs/FCR_DATA/input/viscom_vvision/success