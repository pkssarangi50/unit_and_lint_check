import json
import time
import unittest
from unittest.mock import patch,MagicMock,Mock
from unittest import mock
from pathlib import Path
from apps.heartbeat.heartbeat import Heartbeat
from common.utils import rw_files
from watchdog.observers.polling import PollingObserver
from apps.file_writer import file_writer
from common.utils.config_helper import Config, ConfigLoader
from common.mqtt.mac_mqtt import MacMqttConfiguration,MacMqtt
from common.utils import xlogging

LOGGER = xlogging.getLogger(__name__)

class TestFileWriter(unittest.TestCase):
    
    """Test FileWriter Class to check All functionality"""
    @classmethod
    def setUp(self):
        
        """Setting the File Writer Object"""
        
        self.logger = xlogging.getLogger(self.__module__ + "." + self.__class__.__name__)
        config_file_path= Path(__file__).resolve().parents[0]/ "data"/ "machine-connector-config.json"
        loader = ConfigLoader(config_file_path)
        loader.load_config()
        self.config_object = loader.convert_to_config_object()
        # mqtt_config = MacMqttConfiguration(self.config_object.ie_databus_host,int(self.config_object.ie_databus_port),self.config_object.ie_databus_username,self.config_object.ie_databus_password,'machine-connector','machine-connector')
        self.logger.info('Mqtt Config creation completed')
        # Create an instance of MacMqtt using the mqtt_config
        # self.mqtt_client=MacMqtt(configuration=mqtt_config,logger_basename= LOGGER._logger.name)
        self.mqtt_client=None
        self.hbt=Heartbeat(component_config=self.config_object.writer.component,mqtt_client=self.mqtt_client)
        self.writer_endpoint=self.config_object.writer.endpoints[0]
        self.file_writer=file_writer.FileWriter(self.config_object ,self.mqtt_client,self.writer_endpoint,self.hbt)
        
        
    @patch('watchdog.events.FileSystemEvent')
    def test_file_check_method(self,mocked_event):
        """checking whether the globaldict been Getting Created Or Not"""
        mocked_event.return_value=None
        event = mocked_event
        self.file_writer.file_check(event)
        self.assertEqual(type(file_writer.FileWriter._global_dict),dict)
    
    @patch('apps.heartbeat.heartbeat.Heartbeat.send_heartbeat')
    @patch('apps.file_writer.file_writer.open')
    @patch('apps.file_writer.file_writer.FileWriter.time_difference')
    @patch('common.utils.rw_files.file_forwarding')
    def test_on_payload_received_errior_timeout(self,mock_file_forwarding,mock_time_difference,mock_open,mock_send_hbt):
        """checking the Test for on_payload_received function"""
        #Arrange
        payload:dict = {'fileName': 'test.txt'}
        file_writer.FileWriter._global_dict:dict={'test.txt':1691224242.203368}
        mock_time_difference.return_value=False
        mock_file_forwarding.return_value=None
        mock_open.write.return_value=None
        mock_send_hbt.return_value=None
        #Act
        self.file_writer.on_payload_received(json.dumps(payload).encode())
        #Assert
        self.assertTrue(mock_open.called)
        self.assertTrue(mock_time_difference.called)
        self.assertTrue(mock_file_forwarding.called)
    

    @patch('apps.heartbeat.heartbeat.Heartbeat.send_heartbeat')
    @patch('apps.file_writer.file_writer.open')
    @patch('apps.file_writer.file_writer.FileWriter.time_difference')
    @patch('common.utils.rw_files.file_forwarding')
    def test_on_payload_received_for_success(self,mock_file_forwarding,mock_time_difference,mock_open,mock_send_hbt):
        """checking the Test for on_payload_received function"""
        #Arrange
        payload:dict = {'fileName': 'test.txt'}
        file_writer.FileWriter._global_dict:dict={'test.txt':1691224242.203368}
        mock_time_difference.return_value=True
        mock_file_forwarding.return_value=None
        mock_open.write.return_value=None
        mock_send_hbt.return_value=None
        #Act
        self.file_writer.on_payload_received(json.dumps(payload).encode())
        #Assert
        self.assertTrue(mock_open.called)
        self.assertTrue(mock_time_difference.called)
        self.assertTrue(mock_file_forwarding.called)
    
    
    def test_time_difference_handles_zero_timeout(self):
        
        """
            checking the Positive Scenario over here
        """
        # Arrange
        file_writer.FileWriter._global_dict:dict={'file1':time.time()}
        # Assert
        self.assertTrue(self.file_writer.time_difference('file1'))
    
        
    def test_time_difference_returns_false_when_stored_time_greater_than_current_time(self):
        
        """
        checking the Negetive Scenario over here
        """
        # Arrange
        file_writer.FileWriter._global_dict = {'file1': time.time() - 100}
        # Assert
        self.assertFalse(self.file_writer.time_difference('file1'))
    
    
    @mock.patch('common.mqtt.mac_mqtt.MacMqtt')
    def test_subscribe_success(self, mock_mac_mqtt):
        #Arrange
        self.file_writer.mqtt_client=mock_mac_mqtt
        #Act
        self.file_writer.subscribe_ml_topic()
        #Assert
        self.assertTrue(mock_mac_mqtt.start_communication_and_loop_forever.called)
        
    @patch('watchdog.observers.polling.PollingObserver')
    def test_stop_writer(self, mock_observer):
        # Arrange
        writer = self.file_writer
        mock_observer.return_value=None
        # Act
        writer.stop_writer()
        # Assert
        self.assertEqual(type(writer.stop_writer()),tuple)
    
    
    @patch('apps.file_writer.file_writer.time.sleep',side_effect=StopIteration)
    @patch('apps.file_writer.file_writer.PollingObserver')
    def test_start_writer_exception(self,mock_observer,mock_sleep):
        # Arrange
        mock_observer_instance = mock.Mock()
        mock_observer.return_value = mock_observer_instance
        with self.assertRaises(Exception):
            mock_observer_instance.start.side_effect = Exception
            self.file_writer.start_writer()
        # Assert
        mock_observer.assert_called_once_with()
        
    @patch('apps.file_writer.file_writer.time.sleep',side_effect=StopIteration)
    @patch('apps.file_writer.file_writer.PollingObserver')
    def test_start_writer_keyboard_exception(self,mock_observer,mock_sleep):
        # Arrange
        mock_observer_instance = mock.Mock()
        mock_observer.return_value = mock_observer_instance
        with self.assertRaises(Exception):
            mock_observer_instance.start.side_effect = KeyboardInterrupt
            # mock_observer_instance.join.side_effect=Exception
            self.file_writer.start_writer()
        # Assert
        mock_observer.assert_called_once_with()
        
    def test_stop_writer_stopping(self):
        # Arrange
        writer = self.file_writer
        writer._writer=PollingObserver()
        # Act
        writer.stop_writer()
        # Assert
        self.assertEqual(type(writer.stop_writer()),tuple)
        
        
if __name__ == '__main__':
    unittest.main()
