***Settings***

Library     Collections
Library     String
Library     OperatingSystem
Library    ../utils/start_stop_service.py
Force Tags  All_TC
Default Tags   DEFLT  

***Variables***


***Test Cases***

TC01_Verify Installed FCR Machine Connector App Running Up and Fine 
    [Tags]      functional_mc       tc-887799
    ${RES}=     Verify Installed FCR Machine Connector App Running Up and Fine
    Log To Console  Returned Value of the Machine Connector Application Should Be `0` i.e Success
    Log To Console  Result is --------------------------> ${RES}
    should be equal as numbers  ${RES}  0


TC02_Verify python dependencies created successfully 
    [Tags]    functional_mc    tc-887798
    ${RES}=    Verify python dependencies are created successfully
    Log To Console  Returned Value of the pip validation Should return `0` i.e Success
    Log To Console  Result is --------------------------> ${RES}
    should be equal as numbers  ${RES}  0

TC03_Verify UMC shouldn't work from installation location
    [Tags]    functional_mc    tc-887802
    ${RES}=    Verify umc option shouldn't work from installation location
    Log To Console  Returned Value of the umc validation Should return `True` i.e Success
    Log To Console  Result is --------------------------> ${RES}
    ${RESULT}   Set Variable   Machine Connector And Python of same version already exists
    Should Contain      ${RES}     ${RESULT}
    

***Keywords***

Verify Installed FCR Machine Connector App Running Up and Fine 
    BuiltIn.Log To Console  Installing Machine Connector Application On Background
    [Return]    ${OUT}
    start_stop_service.install_mc
    ${OUT}=     start_stop_service.check_machine_service

Verify python dependencies are created successfully
    BuiltIn.Log To Console  Imc already installed successfully
    [Return]    ${OUT}
    ${OUT}=    start_stop_service.check_pip_requirements

Verify umc option shouldn't work from installation location
    BuiltIn.Log To Console  Trying umc option from installation location
    [Return]    ${OUT}
    ${OUT}=    start_stop_service.install_umc