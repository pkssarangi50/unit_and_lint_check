function Run_TFS_Script {
    $env:SAIL_TEST_ENVIRONMENT="3.1.0"
    $env:SELENIUM_BROWSER="chrome"
    $env:IMPORT_RESULTS = $true
    $env:TFS_TESTPLAN_ID="329279"
    $env:TEST_CONFIGURATION_NAME="SAIL_docker_ubuntu18"
    $env:ARTIFACTS_ROOT="."
    $env:TEST_REPORT_OUTPUT_DIR="."
    ..\..\..\python-mc\python.exe -m sail_test_reporter IMPORT_RESULTS=$IMPORT_RESULTS
}

Run_TFS_Script