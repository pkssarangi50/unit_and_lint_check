import subprocess  # IMPORT FOR SUB PROCESS . RUN METHOD
from subprocess import CalledProcessError
import time

POWERSHELL_PATH = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"  # POWERSHELL EXE PATH
mc_execution_script="..\\..\\..\\generate_mc_win_install_external.ps1 -options {value}"
ps_install_service_script="..\\services\\check_mc_service.ps1"
pip_req_script="..\\services\\check_pip_file.ps1"
upgrade_script="..\\services\\check_umc_service.ps1"
mc_version_script="..\\services\\check_mc_version.ps1"

class Utility:  # SHARED CLASS TO USE IN OUR PROJECT

    @staticmethod    # STATIC METHOD DEFINITION
    def execute_psscript_returncode(script_path, *params):  # SCRIPT PATH = POWERSHELL SCRIPT PATH,  PARAM = POWERSHELL SCRIPT PARAMETERS ( IF ANY )
        try:
            commandline_options = [POWERSHELL_PATH, '-ExecutionPolicy', 'Unrestricted', script_path]  # ADD POWERSHELL EXE AND EXECUTION POLICY TO COMMAND VARIABLE
            process_result = subprocess.run(commandline_options, stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)  # CALL PROCESS
            print(process_result.returncode)  # PRINT RETURN CODE OF PROCESS  0 = SUCCESS, NON-ZERO = FAIL  
            print(process_result.stdout)      # PRINT STANDARD OUTPUT FROM POWERSHELL
            print(process_result.stderr)      # PRINT STANDARD ERROR FROM POWERSHELL ( IF ANY OTHERWISE ITS NULL|NONE )
            if process_result.returncode == 0:  # COMPARING RESULT
                Message = "Success !"
                return process_result.returncode
            else:
                Message = "Error Occurred !"
                return process_result.returncode
        except CalledProcessError as e:
            return e
        # return Message  # RETURN MESSAGE
        
    @staticmethod    # STATIC METHOD DEFINITION
    def execute_psscript_returnstdout(script_path, *params):  # SCRIPT PATH = POWERSHELL SCRIPT PATH,  PARAM = POWERSHELL SCRIPT PARAMETERS ( IF ANY )
        try:
            commandline_options = [POWERSHELL_PATH, '-ExecutionPolicy', 'Unrestricted', script_path]  # ADD POWERSHELL EXE AND EXECUTION POLICY TO COMMAND VARIABLE
            process_result = subprocess.run(commandline_options, stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)  # CALL PROCESS
            print(process_result.returncode)  # PRINT RETURN CODE OF PROCESS  0 = SUCCESS, NON-ZERO = FAIL  
            print(process_result.stdout)      # PRINT STANDARD OUTPUT FROM POWERSHELL
            print(process_result.stderr)      # PRINT STANDARD ERROR FROM POWERSHELL ( IF ANY OTHERWISE ITS NULL|NONE )
            if process_result.returncode == 0:  # COMPARING RESULT
                Message = "Success !"
                return process_result.stdout
            else:
                Message = "Error Occurred !"
                return process_result.stdout
        except CalledProcessError as e:
            return e

    
def set_location():
    Utility.execute_psscript_returncode(mc_execution_script.format(value="p"))
    
def install_mc():
    #set_location()
    Utility.execute_psscript_returncode(mc_execution_script.format(value="imc"))
    
def check_machine_service():
    return Utility.execute_psscript_returncode(ps_install_service_script)


def check_pip_requirements():
    return Utility.execute_psscript_returncode(pip_req_script)

def check_umc_option():
    return Utility.execute_psscript_returncode(upgrade_script)

def check_upgraded_mc_version():
    return Utility.execute_psscript_returnstdout(mc_version_script)

def install_umc():
    #set_location()
    return Utility.execute_psscript_returnstdout(mc_execution_script.format(value="umc"))
    
    
    