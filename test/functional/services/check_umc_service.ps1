function Run_UMC_Script {
    # Expand-Archive -LiteralPath .\FCR-MC-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}.zip -DestinationPath .\FCR-MC-UPGRADE
    # cd .\FCR-MC-UPGRADE
    # Powershell.exe -ExecutionPolicy Unrestricted -File .\generate_mc_win_install_external.ps1 -options imcv
    Powershell.exe -ExecutionPolicy Unrestricted -File .\generate_mc_win_install_external.ps1 -options umc | out-string -stream | select-string -Pattern 'Can not initiate'
}

Run_UMC_Script