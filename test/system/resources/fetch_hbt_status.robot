***Settings***
Library   String
Library   ../utils/python_hbt_sub.py
Library   ../utils/load_test_configs.py
Library   BuiltIn

***Variables***

${CONFIG_FILE_NAME}     integration_config.json

***Keywords***

Checking Robot Functionality 
    [Tags]  heartbeat
    [Documentation]   checking HeartBeat Status code 2 for multiple consition 
    [Arguments]     ${CONFIG_FILE_NAME}
    [Return]        ${ERROR_STATUS_CODE}
    BuiltIn.Log To Console  Config File Name Provided is ${CONFIG_FILE_NAME}
    ${ERROR_STATUS_CODE}=   load_test_configs.status_2_check_multiple_config        ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_STATUS_CODE}


***Test Cases***
Executing HeartBeat TestCases 
    ${ERROR_CODE}=     Checking Robot Functionality   ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}

***Comments***

