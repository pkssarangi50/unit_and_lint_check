*** Settings ***

Library     String
Library     OperatingSystem
Library     ../utils/running_watchdog_automate.py
Library     ../utils/python_mqtt_connection_check.py
Library     ../utils/watchdog_water.py
Library     ../utils/install_mqtt.py
Library     ../utils/start_stop_service.py
Library     ../utils/setup_teardown_for_sys_check.py
Library     ../utils/update_machine_config.py
Library     ../utils/load_config.py

*** Variables ***

${OUT}


*** Keywords ***


MQTT_connection_been_established_Successfully
    Log To Console  MQTT connection Successfully Established and Now Checking for the CSV input File Exists


MQTT_connection_not_been_established
    Log To Console  MQTT connection not Established and Now Checking for the CSV input File Exists

Checking CSV completed Copying the Input File from the Location if not present already
    Log To Console  checking CSV file completed , validating if a copy is needed or not


Stop_FCR_Service
    start_stop_service.stop_fcr_service

Start_FCR_Service
    start_stop_service.start_fcr_service


fetch_folder_location_machine_connector_config
    @{RES}=     Create List  
    @{RES}=     update_machine_config.get_folder_location
    [Return]    @{RES}

Preparing_Required_Configs_for_SystemTest
    Log To Console      Preparing Required Configs for System Test
    Log To Console      Creating Required Folder If Not Created... 
    load_config.load_error_connection_config
    load_config.load_error_conversion_config
    load_config.load_success_config
    load_config.load_success_check_config
    setup_teardown_for_sys_check.create_sub_folder


Stop_change_config_start_fcr_service_explicitly
    [Arguments]     ${PAYLOAD}
    Log To Console    Stoping the FCR Service Explicitly
    start_stop_service.stop_change_config_start_fcr     ${PAYLOAD}
    Sleep   8s
    

RC00_Copy_File_from_config_to_input_location
    [Arguments]     ${SRC}      ${DEST}
    ${PR_VAL}=    running_watchdog_automate.copy_file    ${SRC}      ${DEST}

RC01_Check_Input_Folder_exists_or_not
    [Arguments]  ${FOLDER_NAME}  
    [Return]  ${RES}
    ${RES}=     running_watchdog_automate.check_xml_exists   ${FOLDER_NAME}    


RC02_checking_folder_status_and_checking_time_taken
    [Arguments]     ${SRC}  ${DEST}   ${PATH}   ${CONFIG_LOC}
    ${VAL}=     GET_MQTT_CONNECTION_INFO  ${CONFIG_LOC}
    ${BROKER_ADDRESS}=   Evaluate    ${VAL}.get("IE_DATABUS_HOST") 
    ${BROKER_PORT}=      Evaluate    ${VAL}.get("IE_DATABUS_PORT")
    ${BROKER_USER}=      Evaluate    ${VAL}.get("IE_DATABUS_USERNAME")
    ${BROKER_PASS}=      Evaluate    ${VAL}.get("IE_DATABUS_PASSWORD")
    @{RES}=     Create List  
    @{RES}=     watchdog_water.execute_daemon    ${SRC}    ${DEST}      ${PATH}     ${BROKER_ADDRESS}       ${BROKER_PORT}      ${BROKER_USER}  ${BROKER_PASS}
    [Return]    @{RES}


RC03_stop_mosquitto_service_explicitly
    Log To Console    Stopping the Mosquitto Service Explicitly
    start_stop_service.stop_mosquitto_service


RC04_start_mosquitto_service_explicitly
    Log To Console    Starting the Mosquitto Service Explicitly
    start_stop_service.start_mosquitto_service

RC05_install_mosquitto_broker_explicitly
    Log To Console    Checking If Mosquitto Installation Required Or Not 
    install_mqtt.install_mosquitto


RC06_Get_Folder_File_Location
    @{FOLDER_PATH}=     Fetch Folder Location Machine Connector Config
    ${DEST_LOC}=        Get From List   ${FOLDER_PATH}  0
    ${PATH_LOC}=        Get From List   ${FOLDER_PATH}  0 
    ${FOLDER_NAME}=     Get From List   ${FOLDER_PATH}  1
    ${DEST}=            Get From List   ${FOLDER_PATH}  1
    ${SRC_LOC}=         Get From List   ${FOLDER_PATH}  1
    Set Suite Variable  ${DEST_LOC}     ${DEST_LOC}
    Set Suite Variable  ${PATH_LOC}     ${PATH_LOC}
    Set Suite Variable  ${FOLDER_NAME}     ${FOLDER_NAME}
    Set Suite Variable  ${DEST}     ${DEST}
    Set Suite Variable  ${SRC_LOC}     ${SRC_LOC}


GET_MQTT_CONNECTION_INFO
    [Arguments]     ${CONFIG_LOC}
    [Return]        ${dict_config}
    ${dict_config}=     load_config.load_json_config   ${CONFIG_LOC}  


SS01_SETTING_UP_MQTT_CONNECTION
    [Arguments]     ${BROKER_ADDRESS}   ${BROKER_PORT}      ${BROKER_USER}      ${BROKER_PASS}
    [Return]    ${RES}
    # install_mqtt.install_mosquitto
    Sleep   10s
    ${RES}=     python_mqtt_connection_check.establish_connection   ${BROKER_ADDRESS}       ${BROKER_PORT}      ${BROKER_USER}      ${BROKER_PASS}
    Log To Console      ${RES}
    ${EXP}=     Convert To Boolean  True
    IF    ${RES}==${EXP}
        Log To Console  MQQT Connection Established
    ELSE
        Log To Console  MQQT Connection Not Established
        Log To Console     ${RES}
    END
    Log To Console      Creating the Required Folder Based on the Machine connector Config
    setup_teardown_for_sys_check.create_sub_folder

Replacing Machine Connector Configs
    [Arguments]     ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Config File Name Provided is ${CONFIG_FILE_NAME}
    ${ERROR_STATUS_CODE}=   load_config.replace_configs        ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Putting Dummy Configs into Machine Connector 

*** Comments ***
# resource files is an utility files for the Robot Test Cases and can be used within the Test Script
# for any additional changes and enhancement  can be added to the resource file and refered in the Test Script
