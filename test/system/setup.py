from setuptools import setup, find_packages

required = ['paho-mqtt', 'psycopg2-binary']

setup(name="integration_test", packages=find_packages(),
      install_requires=required)
