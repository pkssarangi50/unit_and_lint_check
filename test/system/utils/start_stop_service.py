import subprocess  # IMPORT FOR SUB PROCESS . RUN METHOD
from subprocess import CalledProcessError
from update_machine_config import updateJsonFile
import time

POWERSHELL_PATH = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"  # POWERSHELL EXE PATH
ps_start_script_path = "..\\service\\start_mosquitto_service.ps1"  # YOUR POWERSHELL FILE PATH
ps_stop_script_path="..\\service\\stop_mosquitto_service.ps1"
ps_start_fcr= "..\\service\\start_fcr_service.ps1"
ps_stop_fcr= "..\\service\\stop_fcr_service.ps1"

class Utility:  # SHARED CLASS TO USE IN OUR PROJECT

    @staticmethod    # STATIC METHOD DEFINITION
    def run_ftp_upload_powershell_script(script_path, *params):  # SCRIPT PATH = POWERSHELL SCRIPT PATH,  PARAM = POWERSHELL SCRIPT PARAMETERS ( IF ANY )
        try:
            commandline_options = [POWERSHELL_PATH, '-ExecutionPolicy', 'Unrestricted', script_path]  # ADD POWERSHELL EXE AND EXECUTION POLICY TO COMMAND VARIABLE
            process_result = subprocess.run(commandline_options, stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)  # CALL PROCESS
            print(process_result.returncode)  # PRINT RETURN CODE OF PROCESS  0 = SUCCESS, NON-ZERO = FAIL  
            print(process_result.stdout)      # PRINT STANDARD OUTPUT FROM POWERSHELL
            print(process_result.stderr)      # PRINT STANDARD ERROR FROM POWERSHELL ( IF ANY OTHERWISE ITS NULL|NONE )
            if process_result.returncode == 0:  # COMPARING RESULT
                Message = "Success !"
            else:
                Message = "Error Occurred !"
        except CalledProcessError as e:
            pass
        # return Message  # RETURN MESSAGE
    
def start_mosquitto_service():
    Utility.run_ftp_upload_powershell_script(ps_start_script_path)

def stop_mosquitto_service():
    Utility.run_ftp_upload_powershell_script(ps_stop_script_path)

def start_fcr_service():
    Utility.run_ftp_upload_powershell_script(ps_start_fcr)

def stop_fcr_service():
    Utility.run_ftp_upload_powershell_script(ps_stop_fcr)
    
def stop_change_config_start_fcr(payload):
    Utility.run_ftp_upload_powershell_script(ps_stop_fcr)
    time.sleep(3)
    updateJsonFile(payload)
    time.sleep(5)
    Utility.run_ftp_upload_powershell_script(ps_start_fcr)
    time.sleep(20)
