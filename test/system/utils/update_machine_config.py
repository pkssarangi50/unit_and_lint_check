import json
import os
import subprocess
from pathlib import Path

def updateJsonFile(payload):
    """
        update the  machine-config.json to update MAX_FILE_SIZE_LIMIT
    """
    jsonFile = open("../../../config/machine-connector-config.json", "r+") # Open the JSON file for reading
    data = json.load(jsonFile) # Read the JSON into the buffer
    jsonFile.truncate(0)
    jsonFile.seek(0)
    data["MAX_FILE_SIZE_LIMIT"] = int(payload)
    json.dump(data, jsonFile, indent=2)
    jsonFile.close()
    
def get_folder_location():
    """ read machine-config.json file and return folder path needed to observe

    Returns:
        _type_: List of Folder Location where file should be observed
    """
    result=[]
    with open("../../../config/machine-connector-config.json") as json_file:
        config_dict=json.load(json_file)
        result.append(config_dict["ENDPOINTS"][0]["reader_path"])
        result.append(os.path.dirname(config_dict["ENDPOINTS"][0]["reader_path"]))
    return result
