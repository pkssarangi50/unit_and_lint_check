import json
import os
import time
import subprocess
import shutil
from pathlib import Path
from subprocess import check_output
from start_stop_service import start_fcr_service


def creat_folder():
    with open("../../../config/machine-connector-config.json") as json_file:
        """reading integration_config.json to know the reader path"""
        dict_path = json.load(json_file)
        reader_path = dict_path["ENDPOINTS"][0]["reader_path"]
        i=1
        while True:
            parent_dir=Path(reader_path).parents[i]
            if os.path.exists(parent_dir):
                return parent_dir
            else:
                i+=1
            


def create_sub_folder():
    """
        creating the folder and subfolder for check 
    """
    base_folder=creat_folder()
    with open("../../../config/machine-connector-config.json") as json_file:
        dict_path = json.load(json_file)
        reader_path = dict_path["ENDPOINTS"][0]["reader_path"]
        os.makedirs(reader_path,exist_ok=True)
        success_path=os.path.join(reader_path,"success")
        os.makedirs(success_path,exist_ok=True)
        error_connection_path=os.path.join(reader_path,"error_connection")
        os.makedirs(error_connection_path,exist_ok=True)
        print(f" Error Connection Path ----> {error_connection_path}")
        error_conversion_path=os.path.join(reader_path,"error_conversion")
        os.makedirs(error_conversion_path,exist_ok=True)
        print(f" Error Connection Path ----> {error_conversion_path}")
