import os.path
from pathlib import Path
import shutil
import os



def check_xml_exists(folder_name):
    """
    Checks if a file exists in the given folder.
    """
    for item in os.listdir(folder_name):
        # print(item)
        if os.path.isfile(os.path.join(folder_name, item)):
            if os.path.splitext(os.path.join(folder_name, item))[1] in [".csv",".CSV"]:
                return True
            else:
                return False
            
def copy_file(src,dest):
    """ Move a file from src to dst.
    Args:
        src (_type_): string location of the source folder
        dest (_type_): string location of the destination folder
    """
    if len(os.listdir(src)):
        for file in os.listdir(src):
            if os.path.splitext(os.path.join(src, file))[1] in [".csv",".CSV"]:
                src_file=os.path.join(src, file)
                shutil.copy(src_file, dest)
                print(f" File {os.path.basename(src)} been copied successfully from {os.path.dirname(src)} to {dest}")
                return "Success"
    else:
        return "Fail"
