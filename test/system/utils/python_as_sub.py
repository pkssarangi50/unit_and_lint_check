import paho.mqtt.client as paho
import time
import json
import sys
from threading import Thread
import zipfile
import logging
import sys
from reading_decompressed_string import decompress_xml
# from update_machine_config import load_json_config


def load_json_config(filepath:str):
    with open(filepath) as json_file:
        config_dict=json.load(json_file)
    return config_dict

topic = load_json_config("../../../config/machine-connector-config.json").get("ENDPOINTS")[0].get("out_topic")


logging.basicConfig(level=logging.INFO, format="%(asctime)s:%(levelname)s:%(message)s")
content_size=None


def on_log(client, userdata, level, buf):
    """
        defining the logging for the buffrer
        
    """
    logging.info(buf)


def on_connect(client, userdata, flags, rc):
    """running the callback function once the connection is established"""
    if rc == 0:
        logging.info("Connected to broker")
        client.connected_flag = True  # Signal connection
        client.subscribe(topic)
    else:
        logging.info("Bad Connection", rc)
        # used when we want to handle the Authentication Error and want to stop the Main While loop
        client.connected_flag = False
        client.disconnect()


def on_message(client, userdata, message):
    """Running the callback function on message received"""
    logging.info(f"******{sys.getsizeof(message.payload.decode('UTF-8'))}*********")
    logging.info(f"******{json.loads(message.payload.decode('UTF-8'))['file_content_compressed']}*********")
    logging.info(f"########{sys.getsizeof(json.loads(message.payload.decode('UTF-8'))['file_content_compressed'])}#######")
    global content_size
    content_size=sys.getsizeof(json.loads(message.payload.decode('UTF-8'))['file_content_compressed'])
    logging.info(f"$$$$$$$ {content_size} $$$$$$$")
    if client.connected_flag and message:
        with open("../data/fetch_data.json", "w") as outfile:
            outfile.write(message.payload.decode("UTF-8"))
        # decompress_xml()
    else:
        client.disconnect() 
    client.disconnect()


def on_disconnect(client, userdata, flags, rc=0, reason="connection Terminated"):
    """ unsubscribe and disconnecting the client"""
    logging.info("closing the MQTT connection on call")
    logging.info(f"Disconnected  Due to {str(rc)}")
    # client.unsubscribe(topic)
    client.loop_stop()


def mqtt_sub(broker_address,broker_port,broker_user,broker_pass):
    """ Subscribing the Messages from MQQt broker on specified topic """
    client = paho.Client("FCR Connectivity Check")
    client.connected_flag=False
    client.stop_main_loop=False
    client.on_log = on_log  # binding the on_log callback
    client.on_connect = on_connect  # binding in_connect callback
    client.on_message = on_message  # binding on_message callback
    client.on_disconnect = on_disconnect  # binding the disconnect callback here
    """ Handling the Graceful Termination in case of Exception whilst connect"""
    try:
        broker = broker_address
        port = int(broker_port)
        MQTT_USER=broker_user
        MQTT_PASS=broker_pass
        client.username_pw_set(MQTT_USER, MQTT_PASS)
        client.connect(broker, port)
        client.loop_start()
        time.sleep(30)
        client.loop_stop()
        client.disconnect() 
    except Exception as e:
        logging.info("can't connect to the Broker")
        client.disconnect()   # returning with the exit code of 1 which is for error
    
    

def start_subscribe(broker_address,broker_port,broker_user,broker_pass):
    """creating subsiquent threads to zip the """
    mqtt_sub(broker_address,broker_port,broker_user,broker_pass)
    return content_size
