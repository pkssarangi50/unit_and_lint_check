import paho.mqtt.client as paho
import time
import json
import sys
import logging
import sys

broker = "localhost"
port = 1883
MQTT_USER="testuser"
MQTT_PASS="testpass"
topic_hbt = "inspection_result"

logging.basicConfig(level=logging.INFO, format="%(asctime)s:%(levelname)s:%(message)s")
error_status_code=None

def on_log(client, userdata, level, buf):
    """
        defining the logging for the buffrer
        
    """
    logging.info(buf)


def on_connect(client, userdata, flags, rc):
    """running the callback function once the connection is established"""
    if rc == 0:
        logging.info("Connected to broker")
        client.connected_flag = True  # Signal connection
        client.subscribe(topic_hbt)
    else:
        logging.info("Bad Connection Connection failed", rc)
        # used when we want to handle the Authentication Error and want to stop the Main While loop
        client.stop_main_loop = True
        client.disconnect()


def on_message(client, userdata, message):
    """Running the callback function on message received"""
    global error_status_code
    logging.info(f"Overall Message Is Of {message.payload.decode('UTF-8')}")
    if message.topic=="aoi/raw_inspection_result/predict_test_reduction_xml":
        hbt_all_msg=json.loads(message.payload.decode('UTF-8'))
        error_status_code=hbt_all_msg["errorCode"]
        logging.info(f"Overall Error Is Of {error_status_code}")
    else:
        client.disconnect() 
    client.disconnect()


def on_disconnect(client, userdata, flags, rc=0, reason="connection Terminated"):
    """ unsubscribe and disconnecting the client"""
    logging.info("closing the MQTT connection on call")
    logging.info(f"Disconnected  Due to {str(rc)}")
    client.unsubscribe(topic_hbt)
    client.loop_stop()


def mqtt_sub():
    """ Subscribing the Messages from MQQt broker on specified topic """
    client = paho.Client("FCR Connectivity Check")
    client.connected_flag=False
    client.stop_main_loop=False
    client.on_log = on_log  # binding the on_log callback
    client.on_connect = on_connect  # binding in_connect callback
    client.on_message = on_message  # binding on_message callback
    client.on_disconnect = on_disconnect  # binding the disconnect callback here
    """ Handling the Graceful Termination in case of Exception whilst connect"""
    try:
        client.username_pw_set(MQTT_USER, MQTT_PASS)
        client.connect(broker, port)
        client.loop_start()
        while not client.connected_flag:
            # logging.info("In the waiting Loop")
            if client.stop_main_loop:
                client.loop_stop()
                client.disconnect()  # leaving the Program
    except Exception as e:
        logging.info("can't connect to the Broker")
        client.disconnect()   # returning with the exit code of 1 which is for error
    time.sleep(25)
    client.loop_stop()
    client.disconnect()

def start_subscribe():
    """creating subsiquent threads to zip the """
    mqtt_sub()
    return error_status_code
