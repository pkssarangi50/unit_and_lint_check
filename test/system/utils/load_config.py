import os
import shutil
import time
import json

def replace_configs(config_name:str):
    """replacing the Custom Config to the Default Configs

    Args:
        config_name (str): config file  which need to be send mqtt topic to check error status code 
    """
    mc_config_path = "../../../config/"
    default_config_name = "machine-connector-config.json"
    source_config_file = f"../config/dummy_configs/{config_name}"
    if os.path.exists(source_config_file):
        shutil.copy(f"../config/dummy_configs/{config_name}",mc_config_path)
        if os.path.exists(mc_config_path+default_config_name) : os.remove(mc_config_path+default_config_name)
        os.rename(mc_config_path+config_name, mc_config_path+default_config_name)
        

def load_json_config(filepath:str):
    with open(filepath) as json_file:
        config_dict=json.load(json_file)
    return config_dict


def load_error_connection_config():
    with open("../../../config/machine-connector-config.json") as json_file:
        config_dict=json.load(json_file)
        config_dict["IE_DATABUS_PORT"]=1880
        config_dict["COMPONENT"]["heartbeat_time_in_sec"]=5
        with open("../config/dummy_configs/error_connection_config.json", "w") as outfile:
            json.dump(config_dict,outfile,indent=4)

def load_error_conversion_config():
    with open("../../../config/machine-connector-config.json") as json_file:
        config_dict=json.load(json_file)
        config_dict["MAX_FILE_SIZE_LIMIT"]=20
        config_dict["COMPONENT"]["heartbeat_time_in_sec"]=5
        with open("../config/dummy_configs/error_conversion_config.json", "w") as outfile:
            json.dump(config_dict,outfile,indent=4)
            
def load_success_check_config():
    with open("../../../config/machine-connector-config.json") as json_file:
        config_dict=json.load(json_file)
        config_dict["COMPONENT"]["heartbeat_time_in_sec"]=5
        with open("../config/dummy_configs/machine-connector-success-hbt.json", "w") as outfile:
            json.dump(config_dict,outfile,indent=4)

def load_success_config():
    with open("../../../config/machine-connector-config.json") as json_file:
        config_dict=json.load(json_file)
        with open("../config/dummy_configs/machine-connector-local.json", "w") as outfile:
            json.dump(config_dict,outfile,indent=4)
