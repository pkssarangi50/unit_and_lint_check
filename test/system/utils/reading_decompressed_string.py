import zlib
import base64
from dicttoxml import dicttoxml
from dict2xml import dict2xml
import lxml.etree as ET
from io import BytesIO
from pprint import pprint
import json
import os
import time
from xml.sax.saxutils import unescape

base64_str = None


# check json_payload_from_MQQT_broker_present_or_not
def check_payload():
    if os.path.exists("..\\data\\fetch_data.json"):
        return True
    else:
        return False


# fetching the compressed base64 string from JSON file
def get_base64():
    """ this function will fetch the string value of the compressed base64 from JSON file """
    with open("../data/fetch_data.json", "r") as json_file:
        dict_data = json.load(json_file)
        return dict_data.get("file_content_compressed")



def decode_decompress(base64_str):
    """base64 decoding and decompressing to bytes format """
    byte_str = base64_str.encode('ascii')
    byte_decode = base64.b64decode(byte_str)
    decom_file = zlib.decompress(byte_decode, -zlib.MAX_WBITS)
    return decom_file


def xmlesc(txt):
    """unescaping the xml special characters"""
    return unescape(txt, entities={"&lt;": "<", "&gt;": ">", "&amp;": "&", "&quot;": '"', "&apos;": "'"})


def create_xml():
    """ creating decompressed XML file and compared it with the input folder  """
    with open("../data/decompressed_input.xml", "wb") as xml_file:
        # calling the function to return the decompressed binary content
        decom_file = decode_decompress(base64_str)
        json_str = decom_file.decode("ascii")
        dict_data = json.loads(json_str)
        xml_str = dict2xml(dict_data)
        clear_data = xmlesc(xml_str)
        clear_byte = clear_data.encode("ascii")
        xml_file.write(clear_byte)


def read_decompressed_xml():
    """compiling decompressed XML generated """
    with open("..\\data\\decompressed_input.xml", "r") as f:
        all_lines = f.readlines()
    return len(all_lines)


def remove_line(fileName, lineToSkip):
    """ Removes a given line from a file """
    with open(fileName, 'r') as read_file:
        lines = read_file.readlines()
    currentLine = 1
    with open(fileName, 'w') as write_file:
        for line in lines:
            if currentLine == lineToSkip:
                pass
            else:
                write_file.write(line)
            currentLine += 1


def insert_line(fileName, line_to_replace):
    with open(fileName, 'r') as file:
        lines = file.readlines()

    if len(lines) > int(line_to_replace):
        lines[line_to_replace] = '<?xml version="1.0" encoding="UTF-8"?>'

    with open(fileName, 'w') as file:
        file.writelines(lines)


def decompress_xml():
    global base64_str
    base64_str = get_base64()
    create_xml()
    remove_line("..\\data\\decompressed_input.xml", 1)
    remove_line("..\\data\\decompressed_input.xml", read_decompressed_xml())
    insert_line("..\\data\\decompressed_input.xml", 0)


