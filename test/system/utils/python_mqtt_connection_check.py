import paho.mqtt.client as mqtt
import time
import logging
import sys
import threading
import json


connection_status=False
logging.basicConfig(level=logging.INFO)

broker="localhost"
port=1883
"""Setting Up the Logger Over Here """


def on_log(client, userdata, level, buf):
    logging.info(buf)


def on_connect(client, userdata, flags, rc):
    """callback Script for MQTT Broker Once MQTT client connected"""
    global connection_status
    if rc==0:
        client.connection_flag=True
        logging.info("Connection Successful")
    else:
        logging.info("Bad Connection",rc)
        client.connection_flag=False
        client.loop_stop()

def on_disconnect(client, userdata, rc):
    logging.info(f"Shutting Down the client Connection with return code of {rc}")

def connection_status_check()->bool:
    """Use to Set The Flag in order to Test"""
    return connection_status

def establish_connection(broker,port,MQTT_USER=None,MQTT_PASS=None):
    """"Establishing the connection to the MQTT Broker and  verify its status """
    client=mqtt.Client("AOI-MQTT-Connection-Check")
    client.connection_flag=False #defing the connection flag Property here
    client.on_log=on_log
    client.on_connect=on_connect
    client.on_disconnect=on_disconnect
    try:
        client.username_pw_set("testuser","testpass")
        client.connect(broker,int(port))
    except Exception as e:
        print("Can't Connect to MQTT broker")
        return client.connection_flag 
        # sys.exit("quitting")
    else:
        client.loop_start()
        #starting the loop
        time.sleep(10)
        client.loop_stop()
        return client.connection_flag
    finally:
        client.loop_stop()

# print(establish_connection(broker,port))