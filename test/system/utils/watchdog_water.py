import glob
import os
import shutil
import time
from pathlib import Path
from python_as_sub import start_subscribe
import threading
from threading import Thread

def move_file(src: str, dst: str):
    """
    Move a file from src to dst based on the location
    """
    if len(os.listdir(src)):
        for file in os.listdir(src):
            if os.path.splitext(os.path.join(src, file))[1] in [".csv",".CSV"]:
                src_file=os.path.join(src, file)
                shutil.move(src_file, dst)
                print(f" File {os.path.basename(src_file)} been copied successfully from {src} to {dst}")
                return "Success"
    else:
        return "Fail"

def find_latest_xml_updated(path:str,filetype:str):
    """
    search the machine vendor folder and its sub folder recursively find latest modified file by its time stamp

    Args:
        filetype (str): type of file pattern we want to match

    Returns:
        _type_: (str) of latest file location based on the modified time
    """
    list_of_files = glob.glob(f'{path}\\**\\*.{filetype}', recursive=True)
    latest_file = max(list_of_files, key=os.path.getmtime)
    return latest_file


class CustomThread(Thread):
    """custom thread will help in getting the return value from the Thread
       which is a sub class of Thread class 
    Args:
        Thread (_type_): _description_
    """
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
 
    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)
             
    def join(self, *args):
        Thread.join(self, *args)
        return self._return


def execute_daemon(src,dest,path,broker_address,broker_port,broker_user,broker_pass):
    """
        creating a Daemon Thread to execute on background for 20 seconds until that if message not subscribed 
        then leave the thread give control back to main thread to execute 
        
    """
    result=[]
    th1=CustomThread(target=start_subscribe,args=(broker_address,broker_port,broker_user,broker_pass))
    th1.start()
    time.sleep(5)
    start_time=time.time()
    move_file(src,dest)
    time.sleep(5)
    content_size=th1.join(40)
    latest_file_path=find_latest_xml_updated(path,"csv")
    print(latest_file_path)
    fetch_folder=Path(latest_file_path).parent.name
    fetched_time=os.path.getmtime(latest_file_path)
    duration=fetched_time-start_time
    result.append(fetch_folder)
    result.append(duration)
    result.append(content_size)
    return result