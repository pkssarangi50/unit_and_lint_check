import os
import shutil
import time

from python_hbt_sub import start_subscribe
from start_stop_service import start_fcr_service, stop_fcr_service
from watchdog_water import CustomThread
from update_machine_config import updateJsonFile, get_folder_location

def replace_configs(config_name:str):
    """replacing the Custom Config to the Default Configs

    Args:
        config_name (str): config file  which need to be send mqtt topic to check error status code 
    """
    mc_config_path = "../../../config/"
    default_config_name = "machine-connector-config.json"
    source_config_file = f"../config/dummy_configs/{config_name}"
    if os.path.exists(source_config_file):
        shutil.copy(f"../config/dummy_configs/{config_name}",mc_config_path)
        if os.path.exists(mc_config_path+default_config_name) : os.remove(mc_config_path+default_config_name)
        os.rename(mc_config_path+config_name, mc_config_path+default_config_name)

def status_3_check_extension()->int:
    """checking Status code 3 from the Specified Topic when in-correct file type been placed into the machine vendor folder

    Returns: Return the Error Status code from the Custom Thread
        _type_: int
    """
    start_fcr_service()
    time.sleep(5)
    reader_path = get_folder_location()
    th1=CustomThread(target=start_subscribe)
    th1.start()
    source_config_file = "../config/dummy_configs/abc.txt"
    shutil.copy(source_config_file,reader_path[0])
    value=th1.join(40)
    replace_configs("machine-connector-local.json")
    start_fcr_service()
    return value

def status_3_check_payload_size()->int:
    """checking Status code 3 from the Specified Topic when Max Payload Size specified been reached 

    Returns: Return the Error Status code from the Custom Thread
        _type_: int
    """
    stop_fcr_service()
    time.sleep(5)
    updateJsonFile(2)
    start_fcr_service()
    time.sleep(5)
    reader_path = get_folder_location()
    th1=CustomThread(target=start_subscribe)
    th1.start()
    source_config_file = "../config/AOI04_220803093223_0840085131.XML"
    shutil.copy(source_config_file,reader_path[0])
    value=th1.join(40)
    replace_configs("machine-connector-local.json")
    start_fcr_service()
    return value

def status_2_check_multiple_config(config_name:str)->int:
    """checking Status code 3 from the Specified Topic when in-correct credential  or unaccessable path or insuffient Permission

    Returns: Return the Error Status code from the Topic 
        _type_: int
    """
    stop_fcr_service()
    time.sleep(5)
    replace_configs(config_name=config_name)
    start_fcr_service()
    time.sleep(10)
    value = start_subscribe()
    replace_configs("machine-connector-local.json")
    start_fcr_service()
    return value

def status_4_check_generic_exception_with_invalid_config(config_name:str)->int:
    """checking Status code 3 from the Specified Topic when in-correct credential  or unaccessable path or insuffient Permission

    Returns: Return the Error Status code from the Topic 
        _type_: int
    """
    value = status_2_check_multiple_config(config_name)
    return value
