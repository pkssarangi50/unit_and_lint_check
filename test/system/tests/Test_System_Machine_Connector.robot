*** Settings ***

Library     Collections
Library     String
Library     OperatingSystem
Resource    ../resources/fetch_file_present.robot
Suite Setup  fetch_file_present.Preparing_Required_Configs_for_SystemTest
Force Tags  All_TC
Default Tags   DEFLT




*** Variables ***

${SRC}              ../config/
${PAYLOAD}          2000000
${PAYLOAD_SIZE}     20
${CONFIG_LOC}      ../../../config/machine-connector-config.json



*** Test Cases ***

TC01_Checking_Success_Condition
    [Documentation]     checking the Happy_path if the MQTT connection Established and CSV exists in input folder it should moved to Success folder in 3s time with publishing the Message
    [Tags]      success_check
    [Setup]     fetch_file_present.RC06_Get_Folder_File_Location
    ${CONFIG_FILE_NAME_ERR_CONN}=    Set Variable    machine-connector-success-hbt.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_ERR_CONN}
    Sleep    10s
    fetch_file_present.RC04_start_mosquitto_service_explicitly
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   20s
    fetch_file_present.RC00_Copy_File_from_config_to_input_location    ${SRC}      ${DEST}
    ${VAL}=     fetch_file_present.GET_MQTT_CONNECTION_INFO    ${CONFIG_LOC}
    ${BROKER_ADDRESS}=   Evaluate    ${VAL}.get("IE_DATABUS_HOST") 
    ${BROKER_PORT}=      Evaluate    ${VAL}.get("IE_DATABUS_PORT")
    ${BROKER_USER}=      Evaluate    ${VAL}.get("IE_DATABUS_USERNAME")
    ${BROKER_PASS}=      Evaluate    ${VAL}.get("IE_DATABUS_PASSWORD")
    Log To Console       \n
    Log To Console       Broker-Address is ------>${BROKER_ADDRESS}   
    Log To Console       Broker-Port is -------->${BROKER_PORT}     
    Log To Console       Broker-User is -------->${BROKER_USER}     
    Log To Console       Broker-Port is --------->${BROKER_PASS} 
    Log To Console       \n
    ${RESULT}=           SS01_SETTING_UP_MQTT_CONNECTION      ${BROKER_ADDRESS}      ${BROKER_PORT}       ${BROKER_USER}   ${BROKER_PASS}
    Log To Console       Connection Established: ${RESULT}
    ${EXP}=     Convert To Boolean  True
    Skip If  ${RESULT}!=${EXP}
    Given MQTT_connection_been_established_Successfully
    When fetch_file_present.RC01_Check_Input_Folder_exists_or_not       ${FOLDER_NAME}
    ${OUTPUT}=     fetch_file_present.RC01_Check_Input_Folder_exists_or_not    ${FOLDER_NAME}
    Then Log To Console        File Present: ${OUTPUT}
    ${EXPECTED}=    Convert To Boolean    True
    Then Should Be True    ${EXPECTED}==${OUTPUT}
    Given Checking CSV completed Copying the Input File from the Location if not present already
    Sleep    10s
    @{OUT}=     fetch_file_present.RC02_checking_folder_status_and_checking_time_taken      ${SRC_LOC}      ${DEST_LOC}     ${PATH_LOC}   ${CONFIG_LOC}
    Log Many     @{OUT}
    ${EXPECTED_FOLDER}=     Set Variable	    success
    ${FOLDER_FETCH}=        Get From List   ${OUT}  0 
    ${DURATION}=        Get From List   ${OUT}  1
    ${PAYLOAD_SIZE}=    Get From List   ${OUT}  2
    Log To Console      \n
    Log To Console      RECEIVED_FOLDER:----> ${FOLDER_FETCH}
    Log To Console      PAYLOAD_SIZE: -----> ${PAYLOAD_SIZE}
    Log To Console      \n
    Skip If     "${PAYLOAD_SIZE}" == "${None}"
    Log To Console       \n
    Log To Console      Time Duration is ====================> ${DURATION}
    Log To Console       \n
    Then Should Be True         ${DURATION}< 3
    IF     ${DURATION}< 3 and ${PAYLOAD_SIZE} < 2000000
        Log To Console      Within the Duration Time 
        Skip If     "${FOLDER_FETCH}"!="${EXPECTED_FOLDER}"
        IF      "${FOLDER_FETCH}"=="${EXPECTED_FOLDER}"
            Log To Console  Folder Moved to Success Folder with publishing the Message Payload Over the Topic  
        END     
    ELSE
        Log To Console     Compression Took Longer Than 3 seconds 
    END

TC02_Checking_error_Connection_Condition
    [Documentation]     checking if  MQTT connection not established then expect the folder in error_connection Folder else skip
    [Tags]      error_connection_check
    fetch_file_present.RC03_stop_mosquitto_service_explicitly
    Sleep   5s
    fetch_file_present.Stop_FCR_Service
    Sleep    10s
    ${CONFIG_FILE_NAME_ERR_CONN}=    Set Variable    error_connection_config.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_ERR_CONN}
    Sleep    10s
    fetch_file_present.Start_FCR_Service
    Sleep   20s
    fetch_file_present.RC06_Get_Folder_File_Location
    Sleep    2s
    ${VAL}=     fetch_file_present.GET_MQTT_CONNECTION_INFO    ${CONFIG_LOC}
    ${BROKER_ADDRESS_ERROR}=      Evaluate    ${VAL}.get("IE_DATABUS_HOST") 
    ${BROKER_PORT_ERROR}=         Evaluate    ${VAL}.get("IE_DATABUS_PORT")
    ${BROKER_USER}=         Evaluate    ${VAL}.get("IE_DATABUS_USERNAME")
    ${BROKER_PASS}=         Evaluate    ${VAL}.get("IE_DATABUS_PASSWORD")
    Log To Console       \n
    Log To Console       Broker-Address is ------>${BROKER_ADDRESS_ERROR}   
    Log To Console       Broker-Port is -------->${BROKER_PORT_ERROR}     
    Log To Console       Broker-User is -------->${BROKER_USER}     
    Log To Console       Broker-Port is --------->${BROKER_PASS} 
    Log To Console       \n
    fetch_file_present.RC00_Copy_File_from_config_to_input_location    ${SRC}      ${DEST}
    ${RES}=     SS01_SETTING_UP_MQTT_CONNECTION     ${BROKER_ADDRESS_ERROR}      ${BROKER_PORT_ERROR}       ${BROKER_USER}   ${BROKER_PASS}
    Log To Console      Connection Established: ${RES}
    ${EXP}=     Convert To Boolean  False 
    Skip If  ${RES}!=${EXP}
    Given MQTT_connection_not_been_established
    When fetch_file_present.RC01_Check_Input_Folder_exists_or_not       ${FOLDER_NAME}
    ${OUTPUT}=     fetch_file_present.RC01_Check_Input_Folder_exists_or_not    ${FOLDER_NAME}
    Then Log To Console       File Present: ${OUTPUT}
    ${EXPECTED}=    Convert To Boolean    True
    Then Should Be True    ${EXPECTED}==${OUTPUT}
    Given Checking CSV completed Copying the Input File from the Location if not present already
    @{OUT}=     fetch_file_present.RC02_checking_folder_status_and_checking_time_taken      ${SRC_LOC}      ${DEST_LOC}     ${PATH_LOC}     ${CONFIG_LOC}
    ${EXPECTED_FOLDER_ERR_CONN}=     Set Variable	    error_connection
    ${ERROR_FOLDER_FETCH}=        Get From List   ${OUT}  0 
    ${DURATION}=    Get From List   ${OUT}  1
    Log To Console      \n
    Log To Console      RECEIVED_FOLDER:----> ${ERROR_FOLDER_FETCH}
    Log To Console      \n
    Log To Console      Time Duration is =======================> ${DURATION}
    Log To Console       \n
    Then Should Be True         ${DURATION}< 3
    IF     ${DURATION}< 3
        Log To Console      Within the Duration Time 
        IF      "${ERROR_FOLDER_FETCH}"=="${EXPECTED_FOLDER_ERR_CONN}"
            Log To Console  Folder Moved to error_connection Folder not publishing the Message Payload Over the Topic
        END        
    ELSE
        Log To Console     Compression Took Longer Than 3 seconds 
    END
    Sleep    5s
    

TC03_Checking_error_conversion_Condition
    [Documentation]     checking the MQTT connection established successfully but the file size is >2MB or Unsupported files Pushed to Machine Vendor 
    [Tags]      error_conversion_check
    fetch_file_present.Stop_FCR_Service
    Sleep    10s
    ${CONFIG_FILE_NAME_ERR_CONV}=    Set Variable    error_conversion_config.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_ERR_CONV}
    Sleep    5s
    fetch_file_present.RC04_start_mosquitto_service_explicitly
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   20s
    fetch_file_present.RC06_Get_Folder_File_Location
    Sleep    5s
    ${VAL}=     fetch_file_present.GET_MQTT_CONNECTION_INFO    ${CONFIG_LOC}
    ${BROKER_ADDRESS_ERR_CONV}=      Evaluate    ${VAL}.get("IE_DATABUS_HOST") 
    ${BROKER_PORT_ERR_CONV}=         Evaluate    ${VAL}.get("IE_DATABUS_PORT")
    ${BROKER_USER}=         Evaluate    ${VAL}.get("IE_DATABUS_USERNAME")
    ${BROKER_PASS}=         Evaluate    ${VAL}.get("IE_DATABUS_PASSWORD")
    ${MYPAYLOAD_SIZE}=      Evaluate    ${VAL}.get("MAX_FILE_SIZE_LIMIT")
    Log To Console       \n
    Log To Console       Broker-Address is ------>${BROKER_ADDRESS_ERR_CONV}   
    Log To Console       Broker-Port is -------->${BROKER_PORT_ERR_CONV}     
    Log To Console       Broker-User is -------->${BROKER_USER}     
    Log To Console       Broker-Port is --------->${BROKER_PASS} 
    Log To Console       Mqtt Payload changed to ------>${MYPAYLOAD_SIZE}
    Log To Console       \n
    fetch_file_present.RC00_Copy_File_from_config_to_input_location    ${SRC}      ${DEST}
    ${RESULT}=     SS01_SETTING_UP_MQTT_CONNECTION      ${BROKER_ADDRESS_ERR_CONV}      ${BROKER_PORT_ERR_CONV}       ${BROKER_USER}   ${BROKER_PASS}
    Log To Console       Connection Established: ${RESULT}
    ${EXP}=     Convert To Boolean  True
    Skip If  ${RESULT}!=${EXP}
    Given MQTT_connection_been_established_Successfully
    When fetch_file_present.RC01_Check_Input_Folder_exists_or_not       ${FOLDER_NAME}
    ${OUTPUT}=     fetch_file_present.RC01_Check_Input_Folder_exists_or_not    ${FOLDER_NAME}
    Then Log To Console      File Present: ${OUTPUT}
    ${EXPECTED}=    Convert To Boolean    True
    Then Should Be True    ${EXPECTED}==${OUTPUT}
    Given Checking CSV completed Copying the Input File from the Location if not present already
    @{OUT}=     fetch_file_present.RC02_checking_folder_status_and_checking_time_taken      ${SRC_LOC}      ${DEST_LOC}     ${PATH_LOC}   ${CONFIG_LOC}
    ${EXPECTED_FOLDER_ERR_CONV}=     Set Variable	    error_conversion
    ${FOLDER_FETCH_ERR_CONV}=        Get From List   ${OUT}  0 
    ${DURATION}=        Get From List   ${OUT}  1
    ${PAYLOAD_SIZE_VAL}=    Get From List   ${OUT}  2
    Log To Console      \n
    Log To Console      RECEIVED_FOLDER:----> ${FOLDER_FETCH_ERR_CONV}
    Log To Console      \n
    Log To Console      Time Duration is ================> ${DURATION}
    Log To Console       \n
    Then Should Be True        ${DURATION}< 3
    IF     ${DURATION}< 3
        Log To Console      Within the Duration Time 
        Skip If     "${FOLDER_FETCH_ERR_CONV}"!="${EXPECTED_FOLDER_ERR_CONV}"
        IF      "${FOLDER_FETCH_ERR_CONV}"=="${EXPECTED_FOLDER_ERR_CONV}"
            Log To Console  Folder Moved to error_conversion Folder without publishing message to the MQTT broker dueto payload size is Higher or Unsupported file format
        END        
    ELSE
        Log To Console     Compression Took Longer Than 3 seconds 
    END
    Sleep    5s
    fetch_file_present.Stop_FCR_Service
    Sleep    5s
    ${CONFIG_FILE_NAME_SUCC}=    Set Variable    machine-connector-success-hbt.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_SUCC}
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   20s

TC04_checking_machine_connector_reconnectivity
    [Documentation]     Stopping Mosquitto Service will Redirect the file to error Connection folder and restarting the Mosquitto service send it back to the success folder
    [Tags]    checking_mqtt_reconnectivity
    ${VAL}=     fetch_file_present.GET_MQTT_CONNECTION_INFO    ${CONFIG_LOC}
    fetch_file_present.Stop_FCR_Service
    Sleep    10s
    fetch_file_present.RC03_stop_mosquitto_service_explicitly
    Sleep   10s
    ${CONFIG_FILE_NAME_ERR}=    Set Variable    error_connection_config.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_ERR}
    Sleep    10s
    fetch_file_present.Start_FCR_Service
    Sleep   20s
    fetch_file_present.RC06_Get_Folder_File_Location
    Sleep    2s
    ${VAL}=     fetch_file_present.GET_MQTT_CONNECTION_INFO    ${CONFIG_LOC}
    ${BROKER_ADDRESS_ERR}=      Evaluate    ${VAL}.get("IE_DATABUS_HOST") 
    ${BROKER_PORT_ERR}=         Evaluate    ${VAL}.get("IE_DATABUS_PORT")
    ${BROKER_USER}=         Evaluate    ${VAL}.get("IE_DATABUS_USERNAME")
    ${BROKER_PASS}=         Evaluate    ${VAL}.get("IE_DATABUS_PASSWORD")
    Log To Console       \n
    Log To Console       Broker-Address is ------>${BROKER_ADDRESS_ERR}   
    Log To Console       Broker-Port is -------->${BROKER_PORT_ERR}     
    Log To Console       Broker-User is -------->${BROKER_USER}     
    Log To Console       Broker-Port is --------->${BROKER_PASS} 
    Log To Console       \n
    fetch_file_present.RC00_Copy_File_from_config_to_input_location    ${SRC}      ${DEST}
    ${RES}=     SS01_SETTING_UP_MQTT_CONNECTION     ${BROKER_ADDRESS_ERR}      ${BROKER_PORT_ERR}       ${BROKER_USER}   ${BROKER_PASS}
    Log To Console      Connection Established: ${RES}
    ${EXP}=     Convert To Boolean  False 
    Skip If  ${RES}!=${EXP}
    Given MQTT_connection_not_been_established
    When fetch_file_present.RC01_Check_Input_Folder_exists_or_not       ${FOLDER_NAME}
    ${OUTPUT}=     fetch_file_present.RC01_Check_Input_Folder_exists_or_not    ${FOLDER_NAME}
    Then Log To Console       File Present: ${OUTPUT}
    ${EXPECTED}=    Convert To Boolean    True
    Then Should Be True    ${EXPECTED}==${OUTPUT}
    Given Checking CSV completed Copying the Input File from the Location if not present already
    @{OUT}=     fetch_file_present.RC02_checking_folder_status_and_checking_time_taken      ${SRC_LOC}      ${DEST_LOC}     ${PATH_LOC}     ${CONFIG_LOC}
    ${EXPECTED_FOLDER_ERR}=     Set Variable	    error_connection
    ${FOLDER_FETCH_ERR}=        Get From List   ${OUT}  0 
    ${DURATION}=    Get From List   ${OUT}  1
    Log To Console      \n
    Log To Console      RECEIVED_FOLDER:----> ${FOLDER_FETCH_ERR}
    Log To Console      \n
    Log To Console      Time Duration is =================> ${DURATION}
    Log To Console       \n
    Then Should Be True         ${DURATION}< 3
    IF     ${DURATION}< 3
        Log To Console      Within the Duration Time 
        IF      "${FOLDER_FETCH_ERR}"=="${EXPECTED_FOLDER_ERR}"
            Log To Console  Folder Moved to error_connection Folder not publishing the Message Payload Over the Topic
        END        
    ELSE
        Log To Console     Compression Took Longer Than 3 seconds 
    END
    Sleep    5s
    fetch_file_present.Stop_FCR_Service
    Sleep    10s
    ############## For Reconnection ########
    ${CONFIG_FILE_NAME_SUCESS_FLD}=    Set Variable    machine-connector-success-hbt.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_SUCESS_FLD}
    Sleep   5s
    fetch_file_present.RC04_start_mosquitto_service_explicitly
    Sleep   10s
    fetch_file_present.Start_FCR_Service
    Sleep   10s
    fetch_file_present.Start_FCR_Service
    Sleep   20s
    fetch_file_present.RC06_Get_Folder_File_Location
    Sleep    5s  
    fetch_file_present.RC00_Copy_File_from_config_to_input_location    ${SRC}      ${DEST}
    ${VAL}=     fetch_file_present.GET_MQTT_CONNECTION_INFO    ${CONFIG_LOC}
    ${BROKER_ADDRESS_SUCCESS}=   Evaluate    ${VAL}.get("IE_DATABUS_HOST") 
    ${BROKER_PORT_SUCCESS}=      Evaluate    ${VAL}.get("IE_DATABUS_PORT")
    ${BROKER_USER}=      Evaluate    ${VAL}.get("IE_DATABUS_USERNAME")
    ${BROKER_PASS}=      Evaluate    ${VAL}.get("IE_DATABUS_PASSWORD")
    Log To Console       \n
    Log To Console       Broker-Address is ------>${BROKER_ADDRESS_SUCCESS}   
    Log To Console       Broker-Port is -------->${BROKER_PORT_SUCCESS}     
    Log To Console       Broker-User is -------->${BROKER_USER}     
    Log To Console       Broker-Port is --------->${BROKER_PASS} 
    Log To Console       \n
    ${RESULT}=           SS01_SETTING_UP_MQTT_CONNECTION      ${BROKER_ADDRESS_SUCCESS}      ${BROKER_PORT_SUCCESS}       ${BROKER_USER}   ${BROKER_PASS}
    Log To Console       Connection Established: ${RESULT}
    ${EXP}=     Convert To Boolean  True
    Skip If  ${RESULT}!=${EXP}
    Given MQTT_connection_been_established_Successfully
    When fetch_file_present.RC01_Check_Input_Folder_exists_or_not       ${FOLDER_NAME}
    ${OUTPUT}=     fetch_file_present.RC01_Check_Input_Folder_exists_or_not    ${FOLDER_NAME}
    Then Log To Console        File Present: ${OUTPUT}
    ${EXPECTED}=    Convert To Boolean    True
    Then Should Be True    ${EXPECTED}==${OUTPUT}
    Given Checking CSV completed Copying the Input File from the Location if not present already
    Sleep    10s
    @{OUT}=     fetch_file_present.RC02_checking_folder_status_and_checking_time_taken      ${SRC_LOC}      ${DEST_LOC}     ${PATH_LOC}   ${CONFIG_LOC}
    Log Many     @{OUT}
    ${EXPECTED_FOLDER_SUCCESS}=     Set Variable	    success
    ${FOLDER_FETCH_SUCCESS}=        Get From List   ${OUT}  0 
    ${DURATION}=        Get From List   ${OUT}  1
    ${SUCCESS_PAYLOAD_SIZE}=    Get From List   ${OUT}  2
    Log To Console      \n
    Log To Console      RECEIVED_FOLDER:----> ${FOLDER_FETCH_SUCCESS}
    Log To Console      PAYLOAD_SIZE: -----> ${SUCCESS_PAYLOAD_SIZE}
    Log To Console      \n
    Skip If     "${SUCCESS_PAYLOAD_SIZE}" == "${None}"
    Log To Console       \n
    Log To Console      Time Duration is ==================> ${DURATION}
    Log To Console       \n
    Then Should Be True         ${DURATION}< 3
    IF     ${DURATION}< 3 and ${SUCCESS_PAYLOAD_SIZE} < 2000000
        Log To Console      Within the Duration Time 
        Skip If     "${FOLDER_FETCH_SUCCESS}"!="${EXPECTED_FOLDER_SUCCESS}"
        IF      "${FOLDER_FETCH_SUCCESS}"=="${EXPECTED_FOLDER_SUCCESS}"
            Log To Console  Folder Moved to Success Folder with publishing the Message Payload Over the Topic  
        END     
    ELSE
        Log To Console     Compression Took Longer Than 3 seconds 
    END
    fetch_file_present.Stop_FCR_Service
    Sleep    5s
    ${CONFIG_FILE_NAME_SUCC_LAST}=    Set Variable    machine-connector-local.json
    fetch_file_present.Replacing Machine Connector Configs  ${CONFIG_FILE_NAME_SUCC_LAST}
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   5s
    fetch_file_present.Start_FCR_Service
    Sleep   20s