***Settings***
Library   String
Library   ../utils/python_hbt_sub.py
Library   ../utils/load_test_configs.py
Library   BuiltIn

***Variables***

***Keywords***

Checking Robot Functionality for Status code 2
    [Arguments]     ${CONFIG_FILE_NAME}
    [Return]        ${ERROR_STATUS_CODE}
    BuiltIn.Log To Console  Config File Name Provided is ${CONFIG_FILE_NAME}
    ${ERROR_STATUS_CODE}=   load_test_configs.status_2_check_multiple_config        ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_STATUS_CODE}

Checking Robot Functionality for Status code 3 for Max Payload Size Reached
    [Return]        ${ERROR_STATUS_CODE}
    ${ERROR_STATUS_CODE}=   load_test_configs.status_3_check_payload_size
    BuiltIn.Log To Console  Error Status Code is ${ERROR_STATUS_CODE}

Checking Robot Functionality for Status code 3 for Incorrect File Type Parsing
    [Return]        ${ERROR_STATUS_CODE}
    ${ERROR_STATUS_CODE}=   load_test_configs.status_3_check_extension
    BuiltIn.Log To Console  Error Status Code is ${ERROR_STATUS_CODE}

Checking Robot Functionality for Status code 4
    [Arguments]     ${CONFIG_FILE_NAME}
    [Return]        ${ERROR_STATUS_CODE}
    BuiltIn.Log To Console  Config File Name Provided is ${CONFIG_FILE_NAME}
    ${ERROR_STATUS_CODE}=   load_test_configs.status_4_check_generic_exception_with_invalid_config        ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_STATUS_CODE}

***Test Cases***
TC01 Executing Status 2 TestCase For Incorrect Or Unaccessable Path
    [Tags]  status-2
    [Documentation]   Test For HeartBeat error Status code 2 for Incorrect or Unaccessable reader Paths
    ${CONFIG_FILE_NAME}=    Set Variable    incorrect_path.json
    ${ERROR_CODE}=     Checking Robot Functionality for Status code 2   ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}
    ${EXPECTED}=  Convert To Integer   2
    Should Be True    ${EXPECTED}==${ERROR_CODE}

TC02 Executing Status 2 TestCase For Incorrect Credentials
    [Tags]  status-2
    [Documentation]   Test For HeartBeat error Status code 2 for Incorrect Username and Password For Shared Location
    ${CONFIG_FILE_NAME}=    Set Variable    incorrect_credential.json
    ${ERROR_CODE}=     Checking Robot Functionality for Status code 2   ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}
    ${EXPECTED}=  Convert To Integer   2
    Should Be True    ${EXPECTED}==${ERROR_CODE}

TC03 Executing Status 2 TestCase For Incorrect Permission
    [Tags]  status-2
    [Documentation]   Test For HeartBeat error Status code 2 for Incorrect Insufficient Permission
    ${CONFIG_FILE_NAME}=    Set Variable    incorrect_permission.json
    ${ERROR_CODE}=     Checking Robot Functionality for Status code 2   ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}
    ${EXPECTED}=  Convert To Integer   2
    Should Be True    ${EXPECTED}==${ERROR_CODE}

TC04 Executing Status 4 TestCase For Generic Exception
    [Tags]  status-4
    [Documentation]   Test For HeartBeat error Status code 4 for Generic Exception
    ${CONFIG_FILE_NAME}=    Set Variable    incorrect_config.json
    ${ERROR_CODE}=     Checking Robot Functionality for Status code 4   ${CONFIG_FILE_NAME}
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}
    ${EXPECTED}=  Convert To Integer   4
    Should Be True    ${EXPECTED}==${ERROR_CODE}

TC05 Executing Status 3 TestCase For Max Message PayLoad Size Reached
    [Tags]  status-3
    [Documentation]   Test For HeartBeat error Status code 3 if Payload Size is More then max_message_payload_size
    ${ERROR_CODE}=     Checking Robot Functionality for Status code 3 for Max Payload Size Reached
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}
    ${EXPECTED}=  Convert To Integer   3
    Should Be True    ${EXPECTED}==${ERROR_CODE}

TC06 Executing Status 3 TestCase For Incorrect File Parsing
    [Tags]  status-3
    [Documentation]   Test For HeartBeat error Status code 3 if Payload Size is More then max_message_payload_size 
    ${ERROR_CODE}=     Checking Robot Functionality for Status code 3 for Incorrect File Type Parsing
    BuiltIn.Log To Console  Error Status Code is ${ERROR_CODE}
    ${EXPECTED}=  Convert To Integer   3
    Should Be True    ${EXPECTED}==${ERROR_CODE}

***Comments***
# If We are Running the Tags with Status Code -2 `status-2` as Tags Then only errorCode 2 will be propagated
# if we are Running the Tags with Status Code -1 `status-3` as Tags then errorCode 3 error code will be propagated

