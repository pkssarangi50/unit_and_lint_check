$32_bit_softwares=Get-ItemProperty "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*" | Select-Object -Property DisplayName,DisplayVersion,publisher,InstallDate
$64_bit_software=Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*" | Select-Object -Property DisplayName,DisplayVersion,publisher,InstallDate
$all_software=$32_bit_softwares+$64_bit_software
Write-Host      "Stoping the Mosquitto Service"
Invoke-Expression "net stop mosquitto"
