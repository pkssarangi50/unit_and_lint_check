from datetime import datetime
import time

from common.utils import rw_files
from common.utils.config_helper import ComponentConfig
from common.utils import xlogging
from common.mqtt.mac_mqtt import MacMqtt

class Heartbeat():
    """The main class of this application."""
    def __init__(self, component_config: ComponentConfig, mqtt_client: MacMqtt):
        self.logger = xlogging.getLogger(
            self.__module__ + "." + self.__class__.__name__)
        self.component_config = component_config
        self.mqtt_client = mqtt_client
        self.last_file_time_stamp = None
        self.last_file_processed = None

    def start_heartbeat(self)-> None:
        '''
        Start Heartbeat
        '''
        try:
            self.logger.info("Heartbeat Initialized")
            while True and self.mqtt_client:
                self.send_heartbeat(1, self.last_file_processed, self.last_file_time_stamp)
                time.sleep(self.component_config.heartbeat_time_in_sec)
        except Exception as e:
            self.logger.error("Generic Error in Start Heartbeat !!!")
            self.logger.error(e)
            self.mqtt_client = None # type: ignore

    def send_heartbeat(self, error_code: int, last_file_processed: str, last_file_time_stamp: datetime)->dict:
        '''
        Sends Heartbeat for different components
        '''
        self.logger.info("Creating message payload for sending heartbeat started")
        msg_payload = {
            "timeStamp": rw_files.format_time_utc_iso8601
                         (datetime.now().timestamp(),
                          self.component_config.heart_beat_time_zone),
            "lastProcessedFile": last_file_processed if last_file_processed != '' else self.last_file_processed,
            "lastFileTimeStamp": last_file_time_stamp if last_file_time_stamp != '' else self.last_file_time_stamp,

            "componentType": self.component_config.component_type,
            "componentVersion": self.component_config.component_version,
            "componentId": self.component_config.component_id,
            "errorCode": error_code
        }
        self.mqtt_client.publish(topic=
                                 self.component_config.heartbeat_topic, message=msg_payload)
        self.logger.info(
            f"Heartbeat send," f"topic: {self.component_config.heartbeat_topic}"
            + f" ,payload: {msg_payload}"
        )
        self.logger.info("Creating message payload for sending heartbeat completed")
        return msg_payload

    def update_last_file_timestamps(self, last_file_processed, last_file_time_stamp)->None:
        """Update Last File Timestamps"""
        self.logger.info("Updating last file timestamps started")
        self.last_file_processed = last_file_processed
        self.last_file_time_stamp = last_file_time_stamp
        self.logger.info("Updating last file timestamps completed")
