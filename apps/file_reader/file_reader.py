import fnmatch
import os
import time
import sys
from datetime import datetime, timedelta
from pathlib import Path

from watchdog.events import PatternMatchingEventHandler
from watchdog.observers.polling import PollingObserver
from apps.heartbeat.heartbeat import Heartbeat

from common.utils import rw_files, xlogging
from common.utils import data_hash
from common.mqtt.mac_mqtt import MacMqtt
from common.utils.config_helper import Config, ReaderEndpoint

class FileReader:
    """File Reader"""

    def __init__(self, app_config: Config, mqtt_client: MacMqtt, endpoint: ReaderEndpoint, heartbeat:Heartbeat):
        """Create new instance."""
        self.app_config = app_config
        self.mqtt_client = mqtt_client
        self.endpoint = endpoint
        self.heartbeat = heartbeat
        self._reader = None
        self.logger = xlogging.getLogger(
            self.__module__ + "." + self.__class__.__name__)

    def process_existing_file(self, dir_path:str):
        """Process existing file"""
        self.logger.info('Process existing started')
        for file in os.listdir(dir_path):
            filename = os.path.join(dir_path, file)
            filename_extension = Path(filename).suffix
            isallowed_extension = any(fnmatch.fnmatch(filename_extension, pattern)
                                      for pattern in self.endpoint.file_extension)
            if os.path.isfile(filename) and isallowed_extension:
                if self.mqtt_client.is_connected():
                    self.file_processing(filename)
                else:
                    rw_files.file_forwarding(filename, self.endpoint.error_connection_path,
                                             os.path.basename(filename))
        self.logger.info('Process existing completed')

    def file_check(self, event):
        """File Check"""
        self.logger.info('File check started')
        self.logger.info(
            f"- {self.endpoint.machine_station_name} file {os.path.basename(event.src_path)} has been recognized at: {self.endpoint.reader_folder_path}")
        self.file_processing(event.src_path)
        self.logger.info('File check completed')

    def file_processing(self, src_path:str):
        """File processing"""
        try:
            self.logger.info('File processing started')
            self.file_name = os.path.basename(src_path)
            if not self.mqtt_client.is_connected():
                self.logger.error(f"File Processing - {self.endpoint.machine_station_name} - MQTT  Client not Connected")
                rw_files.file_forwarding(src_path, self.endpoint.error_connection_path, self.file_name)
            else:
                data_to_publish = self.prepare_result_data(Path(src_path))
                topic = self.endpoint.out_topic

                msg_payload = self.create_msg_payload(data_to_publish, src_path)

                payload_size = sys.getsizeof(msg_payload.get("file_content_compressed"))
                self.logger.info(f"File Processing - {self.endpoint.machine_station_name} - Size of payload {payload_size}")
                if payload_size > self.app_config.max_file_size_limit and self.mqtt_client.is_connected():
                    self.logger.info(
                        f"File Processing - {self.endpoint.machine_station_name} - Maximum payload size exceeded!! Moving file, {self.file_name} to error_conversion..")
                    self.heartbeat.send_heartbeat( 3, self.file_name, self.get_file_creation_timestamp(src_path))
                    rw_files.file_forwarding(src_path, self.endpoint.reader_error_conversion_path, 'exceed_size_'+ self.file_name)
                else:
                    self.logger.info(f"File Processing - {self.endpoint.machine_station_name} - Publishing file")
                    self.mqtt_client.publish(topic=topic, message=msg_payload)
                    self.logger.info(
                        f"File Processing - {self.endpoint.machine_station_name} - Data send, topic: {topic}, file name: {self.file_name}"
                    )
                    self.heartbeat.update_last_file_timestamps(self.file_name, self.get_file_creation_timestamp(src_path))
                    rw_files.file_forwarding(src_path, self.endpoint.reader_success_path, self.file_name)

                self.logger.info(
                    f"File Processing - {self.endpoint.machine_station_name} - Cycle wait time in secs after each xml publish: {self.app_config.cycle_time_in_sec}")
                time.sleep(self.app_config.cycle_time_in_sec)
            self.logger.info('File processing completed')
        except NotImplementedError as e:
            self.logger.error(f"File Processing - {self.endpoint.machine_station_name} - Exception...")
            self.logger.error(
                f"File Processing - {self.endpoint.machine_station_name} - Error while sending {self.endpoint.machine_station_name} logfile: {self.file_name}")
            self.logger.error(e)
            self.heartbeat.send_heartbeat(3, self.file_name, self.get_file_creation_timestamp(src_path))
            rw_files.file_forwarding(src_path, self.endpoint.error_conversion_path, self.file_name)
            self.logger.info(
                f"File Processing - {self.endpoint.machine_station_name} - Cycle wait time in secs after each xml publish: {self.app_config.cycle_time_in_sec}")
            time.sleep(self.app_config.cycle_time_in_sec)
        except Exception as e:
            self.logger.error(f"File Processing - {self.endpoint.machine_station_name} - Generic Error in Processing File Reader !!!")
            self.logger.error(e)
            self.heartbeat.send_heartbeat(4, '', '')

    def get_file_creation_timestamp(self, src_path:str):
        """
        Verify if the time stamp is not null
        If not, then we will provide the same time as the timestamp
        else we will populate the configured timezone current date and time
        """
        return datetime.fromtimestamp(os.path.getctime(src_path)).strftime("%Y-%m-%dT%H:%M:%SZ") if (os.path.getctime(src_path) is not None) else rw_files.format_time_utc_iso8601(datetime.now().timestamp(), self.endpoint.file_content_time_zone)

    def start_reader(self):
        """Start Reader
        Reader when started should hold responsibility of existing and new files
        """
        try:
            self.process_existing_file(self.endpoint.reader_folder_path)
            self.logger.info(f"{self.endpoint.machine_station_name} - Starting File Reader - " + self.endpoint.machine_station_name)
            my_event_handler = PatternMatchingEventHandler(patterns=self.endpoint.file_extension, ignore_patterns=[], ignore_directories=True)
            my_event_handler.on_created = self.file_check
            self._reader = PollingObserver()
            self._reader.schedule(my_event_handler, self.endpoint.reader_folder_path, recursive=False)
            self._reader.start()
            self.logger.info(f"{self.endpoint.machine_station_name} - File Reader started successfully - " + self.endpoint.machine_station_name)

            while True and self._reader:
                time.sleep(1)
        except KeyboardInterrupt:
            if self._reader:
                self._reader.stop()
                self._reader = None
            rw_files.input_quit("{self.endpoint.machine_station_name} - Start File Reader - The app was stopped.")
        except Exception as e:
            self.logger.error(f"{self.endpoint.machine_station_name} - Generic Error in Start File Reader !!!")
            self.logger.error(e)
            if self._reader:
                self._reader.stop()
                self._reader = None
            self.heartbeat.send_heartbeat(4, '', '')
        finally:
            self._reader.join()

    def stop_reader(self):
        """Stop Reader"""
        self.logger.info(f"{self.endpoint.machine_station_name} - Stopping File Reader - " + self.endpoint.machine_station_name)
        self._reader.stop()
        self._reader = None
        self.logger.info(f"{self.endpoint.machine_station_name} - File Reader stopped successfully - " + self.endpoint.machine_station_name)

    def prepare_result_data(self, file_path: Path) -> str:
        """Return a payload str for the specified inspection result test file.
        The method replaces the `inspectionBegin` and `inspectionEnd` with the current time stamp plus 5 seconds.
        """
        self.logger.info(f"Preparing result for file - {file_path}")
        if not os.path.exists(file_path):
            self.logger.error(f"{self.endpoint.machine_station_name} - The file {file_path} does not exist, skipping file")
            return ''

        path = Path(file_path)
        data = None
        if path.suffix.lower() == ".csv":
            data = rw_files.read_data(file_path)
        elif path.suffix.lower() == ".xml":
            data = rw_files.read_data(file_path)
        elif path.suffix.lower() == ".json":
            data = rw_files.load_yaml_or_json(file_path)
            (
                start_timestamp_iso8601,
                end_timestamp_iso8601,
            ) = self.create_time_stamps_iso8601()
            data["inspectionBegin"] = start_timestamp_iso8601
            data["inspectionEnd"] = end_timestamp_iso8601
            data["fileName"] = path.name
            return str(data)
        else:
            raise NotImplementedError(f"file {file_path} was not a .csv, .xml, .json file, skip")
        self.logger.info('Result preparation completed')
        return data

    def create_msg_payload(self, data_to_publish:str, src_path:str):
        """Create the message payload."""
        self.logger.info('Message payload creation started')
        if self.endpoint.compression:
            self.logger.info(f"File Processing - {self.endpoint.machine_station_name} - Compressing msg payload...")
            file_content_compressed = rw_files.compress_in_deflate(data_to_publish)
        else:
            file_content_compressed = data_to_publish

        msg_payload = {
            "file_name": self.file_name,
            "file_content_format": Path(self.file_name).suffix[1:],
            "file_content": data_to_publish,
            "file_content_compressed": file_content_compressed,
            "file_content_time_zone": self.endpoint.file_content_time_zone,
            "file_creation_timestamp": self.get_file_creation_timestamp(src_path),
            "machine_station_name": self.endpoint.machine_station_name,
            "machine_vendor_type": self.endpoint.machine_vendor_type,
            "machine_type": self.endpoint.machine_type,
            "interface_version": self.endpoint.interface_version,
            "trace_id": data_hash.hash_data(data_to_publish)
        }
        self.logger.info('Message payload creation completed')
        return msg_payload

    @staticmethod
    def create_time_stamps_iso8601(dif_sec: int = 5):
        """Creates two timestamps in ISO 8601 with a default difference of 5 seconds."""
        start_time_now = datetime.now()
        end_time = start_time_now + timedelta(seconds=dif_sec)
        start_timestamp_iso8601 = rw_files.format_time_utc_iso8601(start_time_now.timestamp())
        end_timestamp_iso8601 = rw_files.format_time_utc_iso8601(end_time.timestamp())
        return start_timestamp_iso8601, end_timestamp_iso8601