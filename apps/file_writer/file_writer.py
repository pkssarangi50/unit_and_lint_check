import os
import json
import time
from datetime import datetime, timedelta

from watchdog.events import FileSystemEventHandler
from watchdog.observers.polling import PollingObserver
from apps.heartbeat.heartbeat import Heartbeat

from common.utils import rw_files, xlogging
from common.mqtt.mac_mqtt import MacMqtt
from common.utils.config_helper import Config, WriterEndpoint


class FileWriter:
    """File Writer"""

    # Global dictionary to store raw filename and timestamp
    _global_dict = {}

    def __init__(self, app_config: Config, mqtt_client: MacMqtt, endpoint: WriterEndpoint, heartbeat:Heartbeat):
        """Create new instance."""
        self.app_config = app_config
        self.mqtt_client = mqtt_client
        self.endpoint = endpoint
        self._writer = None
        self.heartbeat = heartbeat
        self.logger = xlogging.getLogger(
            self.__module__ + "." + self.__class__.__name__)

    def file_check(self, event):
        """File Check"""
        self.logger.info("Hurray .................... Get the File...................")
        self.logger.info(
            f"- {self.endpoint.machine_station_name} file {os.path.basename(event.src_path)} has"
            + f" been recognized at: {self.endpoint.reader_success_path}")
        FileWriter._global_dict[os.path.basename(event.src_path)]=time.time()
        self.logger.info(f"global dict is of {FileWriter._global_dict}")

    def on_payload_received(self, payload_recieved):
        payload = payload_recieved
        payload = json.loads(payload.decode("utf-8").replace("'",'"'))
        self.logger.info(f"!!!!!!!!!!!!!!!PayLoad is Of {payload}")
        #TODO:herewe will write the code  to subscribe and check the Timeout and send to the SMB Share
        # self.logger.info(f"Payload Received Time is of ---->{current_time}")
        with open(os.path.join(self.endpoint.writer_original_ml_result,payload["fileName"].split('.')[0]),'w') as orig_file:
            json.dump(payload, orig_file, indent=4)
        
        time.sleep(10)
        self.logger.info(f"{type(FileWriter._global_dict)} ######## { FileWriter._global_dict}")
        if payload["fileName"] in FileWriter._global_dict.keys():
            if self.time_difference(payload["fileName"]):
                self.logger.info(" Did not timeout yet !!!")
                # Temp code, TODO: main logic
                rw_files.file_forwarding(self.endpoint.reader_success_path+'\\'+payload["fileName"], self.endpoint.writer_merged_ml_result, payload["fileName"] )
                test = self.endpoint.reader_success_path+'\\'+payload['fileName']
                self.heartbeat.send_heartbeat(1, '', '')
            else:
                self.logger.error(" Timeout out !!!")
                rw_files.file_forwarding(self.endpoint.reader_success_path+'\\'+payload["fileName"], self.endpoint.writer_error_timeout_path, payload["fileName"])
                self.heartbeat.send_heartbeat(3, '', '')
            
    def time_difference(self,filename)-> bool:
        current_time=time.time()
        if (current_time - FileWriter._global_dict[filename]) < self.app_config.writer.max_ml_result_timeout_in_sec:
            return True
        else:
            return False

    def subscribe_ml_topic(self):
        subscription={
            self.endpoint.ml_in_topic:self.on_payload_received
        }
        self.mqtt_client.start_communication_and_loop_forever(subscription,broker_address=self.app_config.ie_databus_host,
        broker_port=self.app_config.ie_databus_port,broker_username=self.app_config.ie_databus_username,broker_password=self.app_config.ie_databus_password)


    def start_writer(self):
        """Start Writer"""
        try:
            self.logger.info(f"{self.endpoint.machine_station_name} - Starting File Writer - " + self.endpoint.machine_station_name)
            # Creating an event handler
            my_event_handler = FileSystemEventHandler()
            # Defining that the file_check method will be called when a new file gets created
            my_event_handler.on_created = self.file_check
            # Creating an reader object
            self._writer = PollingObserver()
            # Define the reader service incl. relevant parameters
            self._writer.schedule(my_event_handler, self.endpoint.reader_success_path, recursive=False)
            # starting the reader
            self._writer.start()
            self.logger.info(f"{self.endpoint.machine_station_name} - File Writer started successfully - " + self.endpoint.machine_station_name)
            while True and self._writer:
                time.sleep(1)
        except KeyboardInterrupt:
            if self._writer:
                self._writer.stop()
                self._writer = None
            rw_files.input_quit(f"{self.endpoint.machine_station_name} - Start File Writer - The app was stopped.")
        except Exception as e:
            self.logger.error(f"{self.endpoint.machine_station_name} - Generic Error in Start File Writer !!!")
            self.logger.error(e)
            if self._writer:
                self._writer.stop()
                self._writer = None
        finally:
            self._writer.join()

    def stop_writer(self, dif_sec: int = 5):
        """Stop Writer"""
        self.logger.info(f"{self.endpoint.machine_station_name} - Stopping File Writer - " + self.endpoint.machine_station_name)
        if self._writer:
            self._writer.stop()
            self._writer = None
        self.logger.info(f"{self.endpoint.machine_station_name} - File Writer stopped successfully - " + self.endpoint.machine_station_name)
        """Creates two timestamps in ISO 8601 with a default difference of 5 seconds."""
        start_time_now = datetime.now()
        end_time = start_time_now + timedelta(seconds=dif_sec)
        start_timestamp_iso8601 = rw_files.format_time_utc_iso8601(start_time_now.timestamp())
        end_timestamp_iso8601 = rw_files.format_time_utc_iso8601(end_time.timestamp())
        return start_timestamp_iso8601, end_timestamp_iso8601
