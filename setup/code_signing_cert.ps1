$subject = "FCR Authenticode-"+(Get-Date -Format "yyyy-mm-ddHH:mm:ss.fff")
$authenticode = New-SelfSignedCertificate -Subject $subject -CertStoreLocation Cert:\LocalMachine\My -Type CodeSigningCert
Get-ChildItem Cert:\LocalMachine\My | Where-Object {$_.Subject -eq $subject}
$codeCertificate = Get-ChildItem Cert:\LocalMachine\My | Where-Object {$_.Subject -eq "CN="+$subject}
$store1 = new-object System.Security.Cryptography.X509Certificates.X509Store("TrustedPublisher","localmachine")
$store1.open("MaxAllowed")
$store1.add($codeCertificate)
$store1.close()
$store = new-object System.Security.Cryptography.X509Certificates.X509Store( [System.Security.Cryptography.X509Certificates.StoreName]::Root, "localmachine" )
$store.open("MaxAllowed")
$store.add($codeCertificate)
$store.close()
Set-AuthenticodeSignature -FilePath .\setup\generate_mc_win_install_external.ps1 -Certificate $codeCertificate