$32_bit_softwares=Get-ItemProperty "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*" | Select-Object -Property DisplayName,DisplayVersion,publisher,InstallDate
$64_bit_software=Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*" | Select-Object -Property DisplayName,DisplayVersion,publisher,InstallDate
$all_software=$32_bit_softwares+$64_bit_software
$destination_path="C:\Program Files\mosquitto"
$current_path=Get-Location
$my_exe_path=$current_path
Write-Host "#######$my_exe_path########"

$install_soft="mosquitto"
$install_dir="$($my_exe_path)\$($install_soft)"
# Set-Location "$install_dir\..\"
Write-Host $install_dir
$install_exe="mosquitto.exe"
# $publisher_exe="mosquitto_pub.exe"
$path_to_exe="$install_dir\$install_exe"
# $path_to_exe="C:\Users\z004pf6p\Downloads\machine_connector\machine_connector\machine_connector\test\integration\service\mosquitto\mosquitto.exe"
# $path_to_mosquitto=
# $topic= "aoi/raw_inspection_result/compressed_predict_test_reduction_xml"
# $host_name="localhost"
# $message="Hello"

# installing the MQTT Broker

$url="https://mosquitto.org/files/binary/win64/mosquitto-2.0.15-install-windows-x64.exe"

if ($all_software.DisplayName -like "*mosquitto*"){
    Write-Output "Mosquiito Already Exists"
    # Get-Service -Name "*mosquitto*" | Start-Service
    # $soft_path=Get-Process -Name "*mosquitto*" | Select-Object -Property Path
    # $mosquitto_cmd=$soft_path.Path
    # Set-Location "$mosquitto_cmd\..\"
    # $comand_to_run="$install_exe --version"
    # Invoke-Expression $comand_to_run


    Set-Location $destination_path
    # $command_to_run=".\mosquitto.exe install"
    # Write-Host  $command_to_run
    # Invoke-Expression ".\mosquitto.exe install"
    $command_to_run="net start mosquitto"
    Write-Host  $command_to_run
    Invoke-Expression "net start mosquitto"
    $command_to_run=".\mosquitto.exe --version"
    Write-Host  $command_to_run
    Invoke-Expression ".\mosquitto.exe --version"

    Exit
}
else{
    
    New-Item $install_dir -ItemType Directory -Force
    (New-Object System.Net.WebClient).DownloadFile($url,$path_to_exe)
    Write-Host  Get-Location
    Write-Host  $path_to_exe
    Start-Process -FilePath $path_to_exe /S -NoNewWindow -Wait -PassThru
    # Set-Location $install_dir
    # Get-Service -Name "*mosquitto*" | Start-Service
    # New-NetFirewallRule -DisplayName 'MQTT port' -Profile 'All' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 1883
    # $soft_path=Get-Process -Name "*mosquitto*" | Select-Object -Property Path
    # $mosquitto_cmd=$soft_path.Path
    Set-Location $destination_path
    $command_to_run="$install_exe install"
    Write-Host  $command_to_run
    Invoke-Expression ".\mosquitto.exe install"
    $command_to_run="net start mosquitto"
    Write-Host  $command_to_run
    Invoke-Expression "net start mosquitto"
    $command_to_run="$install_exe --version"
    Invoke-Expression ".\mosquitto.exe --version"

}