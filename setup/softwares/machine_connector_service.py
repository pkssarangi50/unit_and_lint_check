import servicemanager
import socket
import sys
import win32event
import win32service
import win32serviceutil
import logging
import subprocess
import sys
import os

sys.stdout = sys.stderr = open(os.devnull, 'w')

class MachineConnectorService(win32serviceutil.ServiceFramework):

    _svc_name_ = "FCR-Connector-Service"
    _svc_display_name_ = "FCR-Connector-Service"
    src_path= os.getenv("MACHINE_CONNECTOR_INSTALL_PATH")
    
    machineConnectorCommand = f"set PYTHONPATH={src_path}\\machine_connector && {src_path}\\python-mc\\python.exe {src_path}\\machine_connector\\__main__.py"

    outLogDir = f"{src_path}\\logs"
    outLogName = "machine-connector.log"
    prcid = 0

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, *args)
        self.log('Service Initialized.')
        self.stop_event = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)

    def log(self, msg):
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        
    def sleep(self, sec):
        win32api.Sleep(sec*1000, True)

    def SvcStop(self):
        try:
            self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
            self.log('Service has stopped.')
            win32event.SetEvent(self.stop_event)
            subprocess.Popen("TASKKILL /F /PID {pid} /T".format(pid=self.prcid))
            self.ReportServiceStatus(win32service.SERVICE_STOPPED)
        except Exception as e:
            s = str(e);
            self.log('Exception :'+s)


    def SvcDoRun(self):
        self.ReportServiceStatus(win32service.SERVICE_START_PENDING)
        try:
            self.ReportServiceStatus(win32service.SERVICE_RUNNING)
            self.log('Service is starting.')
            self.main()
            win32event.WaitForSingleObject(self.stop_event, win32event.INFINITE)
            self.log('Service started.')
        except Exception as e:
            s = str(e);
            self.log('Exception :'+s)
            self.SvcStop()

    def main(self):
        if not os.path.isdir(self.outLogDir):
            os.mkdir(self.outLogDir)
        with open(self.outLogDir + '\\' + self.outLogName, "a") as outfile:
            prc = subprocess.Popen(self.machineConnectorCommand, stdout=outfile, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)
            self.prcid = prc.pid

if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(MachineConnectorService)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(MachineConnectorService)