@Echo off

set drive=%~dp0
pushd %drive%

If not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)

set file_location="%drive%\generate_mc_win_install_external.ps1"

Powershell.exe -ExecutionPolicy ByPass -File %file_location% -options h

echo q - Quit
:loop
echo[
set /p "options=Enter Options:"
If "%options%" == "" (echo Please enter options.
) Else (
	Powershell.exe -ExecutionPolicy ByPass -File %file_location% -options "%options%"
)
If not "%options%" == "q" goto :loop

Exit /B
