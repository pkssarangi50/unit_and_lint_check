param (
    [Parameter(Mandatory=$true)][string]$options
)

$pythonUrl = "https://www.python.org/ftp/python/3.9.13/python-3.9.13-amd64.exe"
$installPythonAllUsers = "0"

$machineConnectorServiceName = "FCR-Connector-Service"

# Directory to install local python
$global:pythonInstallDir = $null
$global:machineConnectorSrcDir = $null
$global:pid_cmd= $null

# Config Path
$machineConnectorSrcDirSubDirConfig = "config"
$machineConnectorSrcDirSubDirGenerateSubDirDependencies = "setup\dependencies"
$machineConnectorSrcDirSubDirGenerateSubDirSoftwares = "setup\softwares"

$script:pythonExists = $false
$script:machineConnectorServiceExists = $false

if($options -eq "p"){
	Write-Host "You have chosen option 'p'"
	[System.Environment]::SetEnvironmentVariable('MACHINE_CONNECTOR_INSTALL_PATH',$pwd,'Machine')
}

Function ReadJsonValuesFromFile {
	if($IsMCInstalled){
    	if($pwd -notlike $global:machineConnectorSrcDir)
		{
			$jsonPath = "$pwd\version.json"
		}
	}
	else {
		$jsonPath = "$global:machineConnectorSrcDir\version.json"
	}
    $jsonData = Get-Content -Raw -Path $jsonPath | ConvertFrom-Json
	return $jsonData
}

Function Get-MC-PackageVersion {
    $jsonData= ReadJsonValuesFromFile
	return $jsonData.machine_connector_version
}

Function Get-MC-PythonVersion {
    $jsonData= ReadJsonValuesFromFile
	return $jsonData.python_version
}

#This method will return if the machine connector is instslled or not
Function Is_Mc_Installed{
   return ($null -ne (Get-Service -Name "*FCR*" -ErrorAction SilentlyContinue))
}


# verifies the current version in the file is greater than the installed MC environment variable 
Function MC_Upgrade_Needed{
	$mcInstalledVersion =  [System.Environment]::GetEnvironmentVariable('MACHINE_CONNECTOR_INSTALLED_VERSION','Machine')
	$machineConnectorPackageVersion = Get-MC-PackageVersion
    return [System.Version]$machineConnectorPackageVersion -gt [System.Version]$mcInstalledVersion
}

#if python mc not found returns true
#if python upgrade package version is more than the installed version then returns true
#if python upgrade version is equal or less then returns false
Function MC_Python_InstalleOrUpgradeNeed{
	$mcPythonPackageVersion = Get-MC-PythonVersion
	$pythonNameLoc = $global:pythonInstallDir + "\Python.exe"
		if(Test-Path $pythonNameLoc -PathType Leaf){
			Write-Host "Python installation file Found"
			$pyInstalledVersion = (Get-Item $pythonNameLoc).VersionInfo.ProductVersion
            if([System.Version]$mcPythonPackageVersion -le [System.Version]$pyInstalledVersion)
			{
				return $false
			}
		}else{
			Write-Host "Python installation file NOT Found"
		}	
	return $true
}

Function Get-InputFromUser{
	Write-Host "Checking Environment Variables"
	$src_path = [System.Environment]::GetEnvironmentVariable('MACHINE_CONNECTOR_INSTALL_PATH','Machine')
	if ($src_path -eq $null)
	{
		Write-Host "Environment Variables does not exist."
		$src_path = Read-Host "Enter Installation Path"
		try{
		if ($src_path.Length -eq 0){
			throw "Exception Raised, No path selected"
		}
		}catch{
			$src_error = "Installation path can not be empty.. Please retry and provide proper Installation path"
			Write-Error $src_error
			Exit
		}
		
		# Setting pid of initial batch screen
		$global:pid_cmd=@(Get-Process -Name "cmd" | Select-Object id)[0]

        Write-Host "Creating required directories..."
        if (-not(Test-Path -Path $src_path)) {
            New-Item -ItemType directory -Path $src_path -Force | Out-Null
	    }
		[System.Environment]::SetEnvironmentVariable('MACHINE_CONNECTOR_INSTALL_PATH',$src_path,'Machine')
        $global:machineConnectorSrcDir=$src_path
	}else{
            Write-Host "Environment Variables already exist."
			$global:machineConnectorSrcDir=$src_path
    }
}

# Common Block Code
$global:curr_path = Get-Location
$global:machineConnectorSrcDir = [System.Environment]::GetEnvironmentVariable('MACHINE_CONNECTOR_INSTALL_PATH','Machine')


$dt=Get-Date -Format "yyyy-MM-dd"
$LogName="$global:machineConnectorSrcDir\logs\launch_"+$dt+".log"

if($global:machineConnectorSrcDir -eq $null){
	Get-InputFromUser
	if($pwd -like $global:machineConnectorSrcDir){
		Write-Host "Can not initiate install from extraction path... Please choose different path for installation"
		return
	}
	elseif($options -ne "p"){
		Get-ChildItem -Path $global:curr_path| Copy-Item -Force -Destination $global:machineConnectorSrcDir -Recurse -Container
		Start-Process "$global:machineConnectorSrcDir\launch_external.bat" -ArgumentList noexit
		# Killing initial batch process
		if ($null -ne $global:pid_cmd.id){
			taskkill /F /PID  $global:pid_cmd.id
			$global:pid_cmd=$null
		}
	}
}else{
	Start-Transcript $LogName -Append -NoClobber
	Write-Host "Source folder is $global:machineConnectorSrcDir "
}

$global:pythonInstallDir = "$global:machineConnectorSrcDir\python-mc"
Write-Host "Installation Path is $global:machineConnectorSrcDir"

Function Get-HelpMachineConnectorService{
	Write-Host "Usage:"
	Write-Host "cmc - Check Machine Connector Service"
    $IsMCInstalled = Is_Mc_Installed
	if (!$IsMCInstalled){
		Write-Host "imc - Install Machine Connector Service"
	}elseif($IsMCInstalled){
		if($pwd -like $global:machineConnectorSrcDir){
			Write-Host "Upgrade Machine Connector is available from the extracted source location"
		}else{
			Write-Host "umc - Upgrade Machine Connector Service"
		}
		Write-Host "rmc - Remove Machine Connector Service"
	}
}

Function getEventLogs{
	$LogName = 'Application'
	$EventCount = 3
	
	Write-Host ' '
	Write-Host 'Getting logs'
	Write-Host (Get-Date).ToString('MM/dd/yyyy hh:mm:ss tt')
	Write-Host "Searching for the last $EventCount events of source $machineConnectorServiceName.."
	$evts = Get-EventLog -LogName $LogName -Newest $EventCount -Source $machineConnectorServiceName -ErrorAction SilentlyContinue # | Format-List -Property *
	if($evts){
    		Write-Host ' '
		$i = 1;
		$evts | ForEach-Object -Process {
        		Write-Host "`tPrinting event $i"

        		$obj = $_#$evts | Select-Object -Property TimeGenerated, Message
        		$time = $obj."TimeGenerated"
        		$message = $obj."Message" # or $obj | Select -ExpandProperty "SomeProp"

        		Write-Host "`tTime: $time"
        		Write-Host "`tMessage: $message"
        		Write-Host ' '
        		$i++;
       		}
	} else{
    		Write-Host "No events found with the name $machineConnectorServiceName"
	}
}

Function Check-MachineConnectorService{
	Write-Host "Checking Machine Connector Service Started..."
	if (Test-Path -Path $global:pythonInstallDir) {
		Write-Host "Python exists"
		$script:pythonExists = $true
	}else{
		Write-Host "Python NOT exists"
		$script:pythonExists = $false
	}
	
	if (Get-Service $machineConnectorServiceName -ErrorAction SilentlyContinue)	{
		Write-Host "Machine Connector Service exists"
		$script:machineConnectorServiceExists = $true		
	}else{
		Write-Host "Machine Connector Service NOT exists"
	}
	Write-Host "Checking Machine Connector Service Completed..."
}

Function Install-PythonRequirements{
	$pythonRequirementsWheelDir = $machineConnectorSrcDirSubDirGenerateSubDirDependencies + "\wheelhouse"
	if(Test-Path -Path $pythonRequirementsWheelDir){
		Write-Host "Requirements Wheel folder Found"
	}else{
		Write-Host "Requirements Wheel folder NOT Found"
	}
	& $global:pythonInstallDir\Scripts\pip install -r requirements.txt --no-index --find-links $pythonRequirementsWheelDir
}

Function Install-Python{
	Write-Host "Installing Python Started..."
	#Check-MachineConnectorService
	$IsPythonUpgradeNeeded = MC_Python_InstalleOrUpgradeNeed
	if(!$IsPythonUpgradeNeeded){
		# Remove-Python $true
		Write-Host "Python already exists.. skipping install"
		return
	}
	Write-Host "Checking Python installation file..."
	$pythonNameLoc = $machineConnectorSrcDirSubDirGenerateSubDirSoftwares + "\Python.exe"

	Write-Host "PythonInstallDir:::" + $global:pythonInstallDir
	# These are the silent arguments for the install of python
	# See https://docs.python.org/3/using/windows.html
	$Arguments = @()
	$Arguments += "/i"
	$Arguments += 'InstallAllUsers="' + $installPythonAllUsers + '"'
	$Arguments += 'TargetDir="' + $global:pythonInstallDir + '"'
	$Arguments += 'DefaultAllUsersTargetDir="' + $global:pythonInstallDir + '"'
	$Arguments += 'AssociateFiles="0"'
	$Arguments += 'PrependPath="0"'
	$Arguments += 'Include_doc="0"'
	$Arguments += 'Include_debug="0"'
	$Arguments += 'Include_dev="1"'
	$Arguments += 'Include_exe="1"'
	$Arguments += 'Include_launcher="0"'
	$Arguments += 'InstallLauncherAllUsers="' + $installPythonAllUsers + '"'
	$Arguments += 'Include_lib="1"'
	$Arguments += 'Include_pip="1"'
	$Arguments += 'Include_symbols="0"'
	$Arguments += 'Include_tcltk="1"'
	$Arguments += 'Include_test="1"'
	$Arguments += 'Include_tools="1"'
	$Arguments += "/quiet"

	#Install Python
	Write-Host "Installing Python Started..."
	get-childitem $pythonNameLoc | unblock-file # fix for pop up window while installing python
	Start-Process $pythonNameLoc -ArgumentList $Arguments -Wait

	# Installing dependent libraries
	Install-PythonRequirements

	Write-Host "Installing Python Completed..."
}

Function Remove-Python{
	Param ([boolean]$bypassConnectorCheck)
	Write-Host "Removing Python Started..."
	if(!$bypassConnectorCheck){
		Check-MachineConnectorService
	}
	if($script:pythonExists){
		#Fetching version from local softwares folder and uninstalling
		$pythonNameLoc = $machineConnectorSrcDirSubDirGenerateSubDirSoftwares + "\Python.exe"
		$Arguments = @()
		$Arguments += "/uninstall"
		$Arguments += "/quiet"
		Start-Process $pythonNameLoc -ArgumentList $Arguments -Wait
		if (Test-Path -Path $global:pythonInstallDir) {
            Write-Host "Removing python stale files"
            Remove-Item -Recurse "$global:pythonInstallDir"
        }
		Write-Host "Removing Python Completed..."
	}else{
		Write-Host "Python NOT Found"
	}
}

Function Install-MachineConnectorService{
	Write-Host "Installing Machine Connector Service Started..."
	Check-MachineConnectorService
	$IsMCInstalled = Is_Mc_Installed
	if($IsMCInstalled){
		Write-Host "Machine connector already installed please go for a umc option"
		return
	}
	Install-Python
	try
	{
		$machineConnectorPackageVersion = Get-MC-PackageVersion
		Write-Host "Creating Machine Connector Service Started"
		New-Service -name $machineConnectorServiceName -description "Machine Connector service for FCR v$machineConnectorPackageVersion" -binaryPathName $global:machineConnectorSrcDir\$machineConnectorSrcDirSubDirGenerateSubDirSoftwares\machine_connector_service.exe -displayName $machineConnectorServiceName -startupType Automatic
		Write-Host "Creating Machine Connector Service Completed"
		Write-Host "Starting Machine Connector Service"
		Start-Service -name $machineConnectorServiceName -ErrorAction Stop
		Write-Host "Starting Machine Connector Service Completed"
        [System.Environment]::SetEnvironmentVariable('MACHINE_CONNECTOR_INSTALLED_VERSION',$machineConnectorPackageVersion,'Machine')
		Write-Host "Installing Machine Connector Service Completed..."
	} catch {
		Write-Host "$($_.exception)"
		getEventLogs
	}
}

Function Remove-MachineConnectorService{
	Param ([boolean]$bypassConnectorCheck)
	Write-Host "Removing Machine Connector Service Started..."
	if(!$bypassConnectorCheck){
		Check-MachineConnectorService
	}
	if($script:machineConnectorServiceExists){
		$serviceToRemove = Get-WmiObject -Class Win32_Service -Filter "name='$machineConnectorServiceName'"
		if($serviceToRemove){
			Stop-Service -name $machineConnectorServiceName
			$serviceToRemove.delete()
		}
	}else{
		Write-Host "Machine Connector Service NOT Found"
	}
	Remove-Python
	# Clearing folder structure
	if($options -eq "umc"){
		Write-Host "Clearing folder structure except config file and logs in case of complete upgrade"
		Get-ChildItem -Path "$global:machineConnectorSrcDir" -Exclude ($machineConnectorSrcDirSubDirConfig, 'logs') | Remove-Item -Recurse
	}else{
		Write-Host "Clearing folder structure and preserving logs"
		Get-ChildItem -Path "$global:machineConnectorSrcDir" -Exclude ('logs') | Remove-Item -Recurse
	}
	[Environment]::SetEnvironmentVariable("MACHINE_CONNECTOR_INSTALL_PATH",$null,"Machine")
	[Environment]::SetEnvironmentVariable("MACHINE_CONNECTOR_INSTALLED_VERSION",$null,"Machine")
	Write-Host "Removing Machine Connector Service Completed..."
	Write-host "Logs will be present in the installation path.. Remove other stale files if still exist.."
}

Function Upgrade-MachineConnectorService{
        Param ([boolean]$bypassConnectorCheck)
        if(!$bypassConnectorCheck){
            Check-MachineConnectorService
        }
        # Verifying the machine connector environment variables
        # Taking the bat file launched directory as the source path for update
        $mcUpdatePackagePath = $pwd
        $IsPythonUpgradeNeeded = MC_Python_InstalleOrUpgradeNeed
        $IsMcUpgradeNeeded = MC_Upgrade_Needed
        if(!$IsPythonUpgradeNeeded -and !$IsMcUpgradeNeeded){
            Write-Host "Machine Connector And Python of same version already exists"
            return
        }
		Write-Host "Stopping Machine Connector Service..."
		Stop-Service -name $machineConnectorServiceName
		Write-Host "Stopping Machine Connector Service Completed"
		$machineConnectorPackageVersion = Get-MC-PackageVersion
        if (!$IsPythonUpgradeNeeded -and $IsMcUpgradeNeeded)
        {
            Write-Host "Machine Connector Partially Upgrading"
            # Not Complete upgrade
			# Removing old dependencies
            Remove-Item -Recurse -Force -Path "$global:machineConnectorSrcDir\$machineConnectorSrcDirSubDirGenerateSubDirDependencies" | Out-Null
            # Copying new files
			Get-ChildItem -Path ("$mcUpdatePackagePath") -Exclude ($machineConnectorSrcDirSubDirConfig) | Copy-Item -Force -Destination ("$global:machineConnectorSrcDir")  -Recurse -Container
			# Installing dependent libraries
			Install-PythonRequirements
			# Updating description of FCR service
			& sc.exe description $machineConnectorServiceName "Machine Connector service for FCR v$machineConnectorPackageVersion"
			Write-Host "Upgrading Machine Connector Service Completed.. Current FCR Version v$machineConnectorPackageVersion"
            Write-Host "Starting Machine Connector Service"
            Start-Service -name $machineConnectorServiceName -ErrorAction Stop
            [System.Environment]::SetEnvironmentVariable('MACHINE_CONNECTOR_INSTALLED_VERSION',$machineConnectorPackageVersion,'Machine')
            Write-Host "Starting Machine Connector Service Completed"
        }
        elseif($IsPythonUpgradeNeeded -and $IsMcUpgradeNeeded)
        {
            #Complete upgrade, if python upgrade is needed 
            Write-Host "Machine Connector Completly Upgrading"
            if($global:machineConnectorSrcDir -ne $null){
                Remove-MachineConnectorService $true
                [System.Environment]::SetEnvironmentVariable('MACHINE_CONNECTOR_INSTALL_PATH',$global:machineConnectorSrcDir,'Machine')
                if (-not(Test-Path -Path $global:machineConnectorSrcDir)) {
                    Write-Host "Creating required directories"
                    New-Item -ItemType directory -Path $global:machineConnectorSrcDir -Force | Out-Null
                }
                Get-ChildItem -Path $mcUpdatePackagePath| Copy-Item -Force -Destination $global:machineConnectorSrcDir -Exclude $machineConnectorSrcDirSubDirConfig -Recurse -Container
            }
            try
            {
                #when python upgarade is true, and mc upgrade is true/false
                Write-Host "Upgrading Machine Connector Service Started"
				$mcPythonPackageVersion = Get-MC-PythonVersion
                # Install Python
                Install-Python
				Write-Host "Python version upgraded to $mcPythonPackageVersion"
                #avoid mc installation if mc upgrade is false, just restarts the service
                New-Service -name $machineConnectorServiceName -binaryPathName $global:machineConnectorSrcDir\$machineConnectorSrcDirSubDirGenerateSubDirSoftwares\machine_connector_service.exe -displayName $machineConnectorServiceName -startupType Automatic -Description "Machine Connector service for FCR v$machineConnectorPackageVersion"
                Write-Host "Upgrading Machine Connector Service Completed.. Current FCR Version v$machineConnectorPackageVersion"
                Write-Host "Starting Machine Connector Service"
                Start-Service -name $machineConnectorServiceName -ErrorAction Stop
                [System.Environment]::SetEnvironmentVariable('MACHINE_CONNECTOR_INSTALLED_VERSION',$machineConnectorPackageVersion,'Machine')
                Write-Host "Starting Machine Connector Service Completed"
                
            } catch {
                Write-Host "$($_.exception)"
                getEventLogs
            }
        }
        elseif($IsPythonUpgradeNeeded -and !$IsMcUpgradeNeeded){
            # Install Python
			$mcPythonPackageVersion = Get-MC-PythonVersion
            Install-Python
			Write-Host "Python version upgraded to $mcPythonPackageVersion"
			Write-Host "Starting Machine Connector Service"
			Start-Service -name $machineConnectorServiceName -ErrorAction Stop
			Write-Host "Starting Machine Connector Service Completed"
        }
        Write-Host "Upgrading Machine Connector Service Completed..."
}

$options.Split(" ") | ForEach-Object {
    $IsMCInstalled = Is_Mc_Installed
    if($_ -eq "h"  -or $_ -eq "--help"){
		Write-Host "You have chosen option 'h'"
		Get-HelpMachineConnectorService
	}
	if($_ -eq "cmc"){
		Write-Host "You have chosen option 'cmc'"
		Check-MachineConnectorService
	}
	if($_ -eq "ip"){
		Write-Host "You have chosen option 'ip'"
		Install-Python
	}
    #When we are removing the MC python we should be checking is mc removed or not
	if($_ -eq "rp" -And !$IsMCInstalled){
		Write-Host "You have chosen option 'rp'"
		Remove-Python $false
	}
	if($_ -eq "imc"){
        Write-Host "You have chosen option 'imc'"
        Install-MachineConnectorService
	}
	if($_ -eq "umc"){
		Write-Host "You have chosen option 'umc'"
        if($IsMCInstalled){
            if($pwd -like $global:machineConnectorSrcDir){
                Write-Host "Can not initiate upgrade from installation path... Please choose the launch.bat file from the extracted source location to upgrade"
            }else{
                Upgrade-MachineConnectorService
            }
        }
        else{
            Write-Host "Machine connector is not installed please go with imc option"
        }
		
	}
	if($_ -eq "rmc"){
        Write-Host "You have chosen option 'rmc'"
        if($IsMCInstalled){
            Remove-MachineConnectorService
        }
		else{
            Write-Host "Machine connector is not installed please go with imc option"
        }
	}
}


Function IncrementAppVersion {
    param (
        [Parameter(Mandatory = $true)][string]$JsonFilePath,
		[Parameter(Mandatory = $true)][string]$AppName
    )
    #Read the JSON file
    $jsonContent = Get-Content -Raw -Path $JsonFilePath | ConvertFrom-Json

    #Fetch the Machine Connector app version
    $appVersionKey = $AppName
    $currentVersion = $jsonContent.$appVersionKey
    Write-Host "Current app version: $currentVersion"

    #Split the current version into its individual components
    $versionComponents = $currentVersion -split '\.'

    #Increment the build number to +1
    $buildNumber = [int]$versionComponents[3]
    $newBuildNumber = $buildNumber + 1

    #Update the app version with the new build number
    $versionComponents[3] = $newBuildNumber.ToString()

    #Join the version components back into a single string
    $newVersion = $versionComponents -join '.'

    #Update the app version in the JSON object
    $jsonContent.$appVersionKey = $newVersion

    #Convert the JSON object back to a string
    $newJsonContent = $jsonContent | ConvertTo-Json -Depth 10

    #Update the JSON file with the new content
    $newJsonContent | Set-Content -Path $JsonFilePath
    Write-Host "JSON file updated with the incremented version: $newVersion"
}

if($options -eq "imcv"){
	Write-Host "You have chosen option 'imcv'"
	#Call the function with the JSON file path
	IncrementAppVersion -JsonFilePath "$pwd\version.json" -AppName "machine_connector_version"
}

try{
	Stop-Transcript
}catch{
	Write-Host "Transcript already stopped!"
}
