param (
    [Parameter(Mandatory=$true)][string]$options
)

$global:pythonInstallDir = "C:\ProgramData\scoop\apps\python39\3.9.13"
$global:machineConnectorSrcDir = "C:\builds\MAC_Lab\implementation\fcr\machine-connectors"

$machineConnectorSrcDirSubDirGenerateSubDirDependencies = "setup\dependencies"
$machineConnectorSrcDirSubDirGenerateSubDirSoftwares = "setup\softwares"
$machineConnectorSystemTestDir = "test\system"
$extracted_zip = "FCR-MC"
$extracted_zip_python = "$global:machineConnectorSrcDir\$extracted_zip\python-mc"
$copyrightSymbol = [char]0x00A9

Function ReadJsonValuesFromFile {
	$jsonPath = ".\version.json"
	$jsonData = Get-Content -Raw -Path $jsonPath | ConvertFrom-Json
	return $jsonData
}

Function Get-MC-PackageVersion {
    $jsonData = ReadJsonValuesFromFile
    $versionComponents = $jsonData.machine_connector_version -split '\.'
    $versionTuple = [Tuple]::Create($versionComponents[0], $versionComponents[1], $versionComponents[2], $versionComponents[3])
    return $versionTuple
}

$package_version_tuple = Get-MC-PackageVersion
$package_version = $package_version_tuple -replace "[\(\)\s]", "" -replace ",", "."

# Define the version information
$versionInfo = @"
VSVersionInfo(
  ffi=FixedFileInfo(
    filevers=$package_version_tuple,
    prodvers=$package_version_tuple,
    mask=0x0,
    flags=0x0,
    OS=0x4,
    fileType=0x1,
    subtype=0x0,
    date=(0, 0)
  ),
  kids=[
    StringFileInfo(
      [
        StringTable(
          '040904e4',
          [
            StringStruct('CompanyName', 'SIEMENS AG'),
            StringStruct('FileDescription', 'Machine Connector Installer$($copyrightSymbol)'),
            StringStruct('FileVersion', '$package_version'),
            StringStruct('InternalName', 'MC_Setup'),
            StringStruct('LegalCopyright', '$($copyrightSymbol)Siemens AG, 2008-2023'),
            StringStruct('OriginalFilename', 'machine_connector_service.exe'),
            StringStruct('ProductName', 'Siemens Machine Connector$($copyrightSymbol)'),
            StringStruct('ProductVersion', '$package_version')
          ]
        )
      ]
    ),
    VarFileInfo([VarStruct('Translation', [1031, 1200])])
  ]
)
"@

# Save the version information to the file
$versionInfo | Out-File -FilePath $machineConnectorSrcDirSubDirGenerateSubDirSoftwares\exe_version_info.txt -Encoding UTF8 -NoNewLine 

Function Install-Mqtt{
	Write-Host "Installing MQTT"
	Powershell.exe -ExecutionPolicy ByPass -File $global:machineConnectorSrcDir\$extracted_zip\$machineConnectorSrcDirSubDirGenerateSubDirSoftwares\install_mqtt.ps1
}

Function Run-System-Tests-MachineConnectorService{
	Write-Host "Run System Tests Machine Connector Service Started..."
	& $extracted_zip_python\Scripts\pip install -r $global:machineConnectorSrcDir\$extracted_zip\$machineConnectorSystemTestDir\requirements-systemtest.txt
	$reports_path = "$global:machineConnectorSrcDir\$extracted_zip\$machineConnectorSystemTestDir\reports"
	if (-not(Test-Path -Path $reports_path)) {
		New-Item -ItemType directory -Path $reports_path -Force | Out-Null
	}
	$test_location="$global:machineConnectorSrcDir\$extracted_zip\$machineConnectorSystemTestDir\tests"
	Set-Location    $test_location
	$execute_command="$extracted_zip_python\Scripts\robot.exe --exitonfailure --nostatusrc -d $test_location\..\reports .\Test_System_Machine_Connector.robot"
	Invoke-Expression   $execute_command
	Write-Host "Run System Tests Machine Connector Service Completed..."
}

Function Generate-MachineConnectorService{
	if(Test-Path ("setup\generate-requirements.txt") -PathType Leaf){
		Write-Host "Builder Requirements Txt Found"
		Write-Host "Checking Requirements Wheel folder..."
		$pythonRequirementsWheelNotFound = $true
		$pythonRequirementsWheelDir = $machineConnectorSrcDirSubDirGenerateSubDirDependencies + "\wheelhouse"
		if(Test-Path -Path $pythonRequirementsWheelDir){
			Write-Host "Requirements Wheel folder Found"
			$pythonRequirementsWheelNotFound = $false
		}else{
			Write-Host "Requirements Wheel folder NOT Found"
		}
		if($pythonRequirementsWheelNotFound){
			if(Test-Path ("setup\generate-requirements.txt") -PathType Leaf){
				& $global:pythonInstallDir\Scripts\pip download -r setup\generate-requirements.txt -d $pythonRequirementsWheelDir
			}
			if(Test-Path (".\requirements.txt") -PathType Leaf){
				& $global:pythonInstallDir\Scripts\pip download -r .\requirements.txt -d $pythonRequirementsWheelDir
			}
		}
		Write-Host "Installing Builder Requirements Started"
		& $global:pythonInstallDir\Scripts\pip install -r setup\generate-requirements.txt --no-index --find-links $pythonRequirementsWheelDir
		Write-Host "Installing Builder Requirements Completed"
		Write-Host "Building EXE Started"
		& $global:pythonInstallDir\Scripts\pyinstaller -F --hidden-import=win32timezone --distpath $machineConnectorSrcDirSubDirGenerateSubDirSoftwares  $machineConnectorSrcDirSubDirGenerateSubDirSoftwares\machine_connector_service.py --version-file=$machineConnectorSrcDirSubDirGenerateSubDirSoftwares\exe_version_info.txt 
		Write-Host "Building EXE Completed"
		Write-Host "Generating Machine Connector Service Completed..."

		Write-Host "Packaging Machine Connector Service Started..."
		# Create a zip file with the contents
		New-Item -ItemType directory -Path $global:machineConnectorSrcDir -Force | Out-Null
		Powershell.exe -ExecutionPolicy ByPass -File .\setup\code_signing_cert.ps1
		$tempFolderPath = Join-Path $Env:Temp "machine_connector"
		$tempFolderPathSetup = Join-Path $tempFolderPath "setup"
		$tempFolderPathSetupSoftwares = Join-Path $tempFolderPathSetup "softwares"
		New-Item -Type Directory -Path $tempFolderPath | Out-Null
		New-Item -Type Directory -Path $tempFolderPathSetup | Out-Null
		New-Item -Type Directory -Path $tempFolderPathSetupSoftwares | Out-Null
		if ( $options -eq "gmc"){
			Copy-Item -Path (Get-Item -Path ".\*" -Exclude ("build", "machine_connector_service.spec", "install.ps1", "_4dev", "apps", "common", "config", "docs", "test", "setup", ".gitignore", ".gitlab-ci.yml", "README.md", "RELEASE.md")).FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\apps").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\common").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\config").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\docs").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\test").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\setup\*" -Exclude ("launch_external.bat")).FullName -Destination "$tempFolderPathSetup" -Recurse -Force
		}elseif($options -eq "gmc-release"){
			Copy-Item -Path (Get-Item -Path ".\*" -Exclude ("build", "machine_connector_service.spec", "install.ps1", "_4dev", "apps", "common", "config", "docs", "test", "setup", ".gitignore", ".gitlab-ci.yml", "README.md", "RELEASE.md")).FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\apps").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\common").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\config").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\docs").FullName -Destination "$tempFolderPath" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\setup\*" -Exclude ("softwares", "generate_mc_win_install_internal.ps1", "code_signing_cert.ps1", "generate-requirements.txt", "launch_internal.bat")).FullName -Destination "$tempFolderPathSetup" -Recurse -Force
			Copy-Item -Path (Get-Item -Path ".\setup\softwares\*" -Exclude ("exe_version_info.txt", "install_mqtt.ps1","mosquitto.exe")).FullName -Destination "$tempFolderPathSetupSoftwares" -Recurse -Force
		}
		Get-ChildItem $tempFolderPath\*
		Compress-Archive -Path $tempFolderPath\* -DestinationPath $global:machineConnectorSrcDir\FCR.zip
		Remove-Item -Recurse -Force -Path $tempFolderPath | Out-Null

		Write-Host "ZIP Location: " + $global:machineConnectorSrcDir + "\FCR.zip"
		Write-Host "Packaging Machine Connector Service Completed..."
	}else{
		Write-Host "Requirements Txt NOT Found. Please make sure source path is set properly and it matches the path where you have extracted your code"
	}
}

Function Get-HelpMachineConnectorService{
	Write-Host "Usage:"
	Write-Host "gmc - Generate Machine Connector Service Development"
	Write-Host "gmc-release - Generate Machine Connector Service Release"
	Write-Host "im - Install MQTT"
	Write-Host "rs - Run System Tests"
}

$options.Split(" ") | ForEach-Object {
	if($_ -eq "h"  -or $_ -eq "--help"){
		Write-Host "You have chosen option 'h'"
		Get-HelpMachineConnectorService
	}
   	if($_ -eq "gmc"){
		Write-Host "You have chosen option 'gmc'"
		Generate-MachineConnectorService
	}
	if($_ -eq "gmc-release"){
		Write-Host "You have chosen option 'gmc-release'"
		Generate-MachineConnectorService
	}
	if($_ -eq "im" ){
		Write-Host "You have chosen option 'im'"
		Install-Mqtt
	}
	if($_ -eq "rs"){
		Write-Host "You have chosen option 'rs'"
		Run-System-Tests-MachineConnectorService
	}
}