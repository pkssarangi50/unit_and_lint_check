class NetworkShareUnAccessableException(Exception):
    '''
    Class that handles the Network Share UnAccessable Exception
    '''
    def __init__(self, message=""):
        self.message = f"Please check the network connection as path {message} is unaccessable "
        super().__init__(self.message)
    def __str__(self):
        return self.message

class NetworkShareCredentialsException(Exception):
    '''
    Class that handles the Network Share Wrong Credentials Exception
    '''
    def __init__(self, message=""):
        self.message = f"Please check the network share {message} creadentials"
        super().__init__(self.message)
    def __str__(self):
        return self.message

class FileForwardingOsErrorException(Exception):
    '''
    Class that handles the File Forwarding OsError Exception
    '''
    def __init__(self, file_name="", move_to_path="", exceptiontype=""):
        self.file_name = file_name
        self.move_to_path = move_to_path
        self.exceptiontype = exceptiontype
        self.message = f"- Failed to move logfile {self.file_name} to \
            {self.move_to_path} because of {self.exceptiontype}"
        super().__init__(self.message)
    def __str__(self):
        return self.message

class NetworkShareIsWriteProtectedException(Exception):
    '''
    Class that handles the OsError in FileForwarding
    '''
    def __init__(self, message=""):
        self.message = f"Network share {message} is write protected"
        super().__init__(self.message)
    def __str__(self):
        return self.message
