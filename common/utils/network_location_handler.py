import win32wnet
import os
from common.utils import xlogging

LOGGER = xlogging.getLogger(__name__)

def mount_network_location(path:str,username:str,password:str, is_mount_location:bool = False)->bool:
    '''
    Network Location Path Mount Started
    '''
    if is_mount_location:
        LOGGER.info("Network location path mounted started.")
        try:
            net_resource = win32wnet.NETRESOURCE()
            net_resource.lpRemoteName = path
            win32wnet.WNetAddConnection2(net_resource, UserName= username, Password= password)
            LOGGER.info("Network location path mounted successfully.")
            return True
        except Exception as e:
            LOGGER.error("Failed to mount network location path.")
            return False
    else:
        LOGGER.info("Validating local configured path started.")
        # Check if the path is a valid directory
        if os.path.isdir(path):
            LOGGER.info("Configured location path is a valid directory.")
            return True
        else:
            LOGGER.error("Configured location path is not a valid directory.")
            return False

def dispose_network_location(path):

    try:
        flags = 1
        win32wnet.WNetCancelConnection2(path, flags, True)
        LOGGER.info("Network location path connection disposed successfully.")
    except Exception as e:
        LOGGER.info("Existing Network path not available.")


