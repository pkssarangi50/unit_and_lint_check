"""A custom logging module based on the Python logging module.

Importing this module initializes the root logger (if not already configured).
This module provides default settings, which can be replaced with an optional
"./logging.conf" file.

This module provides:

- the custom logger class `XLogging` that wraps a logging.Logger
- the factory function `getLogger()` that creates and caches named instances
- `create_logger_factory()` to create factory functions for XLogging subclasses

Usage:

>>> import xlogging
>>> logger = xlogging.getLogger(__name__)
>>> logger.warning("something special happened...")
"""

from enum import Enum
import json
import logging
import logging.config
from typing import Callable, Type

__all__ = ['getLogger', "XLogger", "create_logger_factory"]


class OneLineExceptionFormatter(logging.Formatter):
    '''
    Xlogger OneLineExceptionFormat for logging data
    '''
    def formatException(self, exc_info):
        result = super().formatException(exc_info)
        return repr(result)

    def format(self, record):
        result = super().format(record)
        if record.exc_text:
            result = result.replace("\n", "")
        return result


DEFAULT_FORMAT = "%(asctime)s | %(levelname)-8s | %(name)s | %(message)s"
default_handler = logging.StreamHandler()
default_formatter = OneLineExceptionFormatter(DEFAULT_FORMAT)
default_handler.setFormatter(default_formatter)

# Set initial logging configuration
# Default config: use it if _CONFIG_FILE doesn't exist
# Use this module's default configuration
logging.basicConfig(level=logging.INFO, handlers=[default_handler])


# log python warnings (like ImportWarning, DeprecationWarning) as WARNING
# using our log format (might be multi-line though!)
logging.captureWarnings(capture=True)


class XLogger:
    """A custom logger that provides a subset of the methods from
    logging.Logger with additional methods to log complexer objects.

    - internally wraps a logging.Logger as self._logger
    - most methods directly delegate to their counterparts on self._logger,
      therefore docstrings of these methods are kept small here:
      see the official logging.Logger documentation for all details
    - it should not be necessary to access self._logger for ordinary use,
      but it can be accessed (rarely) for advanced features
    """

    class Level(Enum):
        '''
        XLogger levels
        '''
        NOTSET = logging.NOTSET      # 0
        DEBUG = logging.DEBUG        # 10
        INFO = logging.INFO          # 20
        WARNING = logging.WARNING    # 30
        ERROR = logging.ERROR        # 40
        CRITICAL = logging.CRITICAL  # 50

    # XXX xlogging.getLogger(name) accepts None but no other false values
    #   -> also enforce the same here?
    # TODO ... or only allow named loggers?
    #   (root logger not needed for logging but how to set loglevel maybe for
    #  configuration?)
    def __init__(self, name: str = None):
        """Create a new instance with a wrapped logging.Logger."""
        self._logger = logging.getLogger(name)

    def debug(self, msg, *args, **kwargs):
        """Log message with level DEBUG on this logger."""
        self._logger.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        """Log message with level INFO on this logger."""
        self._logger.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        """Log message with level WARNING on this logger."""
        self._logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        """Log message with level ERROR on this logger."""
        self._logger.error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        """Log message with level CRITICAL on this logger."""
        self._logger.critical(msg, *args, **kwargs)

    def set_level(self, level):
        """Set the threshold for this logger to `level`.

        :param level: any enum member of self.Level, or it's name or value
        :raise ValueError: if level is invalid
            (doesn't accept arbitrary int values like logging.Logger)
        """
        if isinstance(level, self.Level):
            self._logger.set_level(level.value)
        elif not isinstance(level, (int, str)):
            # Preserve behavior of logging.Logger
            raise TypeError(f"not a Level, an int, or a str: {level}")
        else:
            try:
                self._logger.set_level(self.Level[level].value)
            except KeyError:
                # may raise "ValueError: '{level}' is not a valid Level"
                self._logger.set_level(self.Level(level).value)

    # TODO Expect member of enum Level instead of level_method?
    def log_vars_py(self, level_method, msg, **vars):
        """Log `msg` using `level_method` followed by the name-value pairs
        of the `vars` dict encoded in Python variable assignment style.

        E.g.
        >>> log_vars_py(logger.info, "message42", i=11, f=22.5, b=True, d={"k": "v"})
        logs the following encoded message at INFO:
        ":::PYTHON::: message42: i=11; f=22.5; b=True; d={'k': 'v'}"

        NOTE The output format of this method might be better readable by
          humans than the JSON encoded output from log_vars(); this method
          can log name-value pairs of any value type if it isn't necessary to
          recreate an object with the same value from its repr()
        """
        level_method(":::PYTHON::: %s: %s", msg,
                     "; ".join("%s=%r" % it for it in vars.items()))

    # TODO Expect member of enum Level instead of level_method?
    # XXX The Logstash JSON filter plugin requires that:
    #   - all lists in vars must either contain only numbers or only strings
    #   - the JSON string mustn't contain NaN or Infinity: must remove or
    #     replace these special float values before json.dumps(), e.g.
    #     apply numpy.nan_to_num(array_like).tolist() before passing array_like
    #     to this method (if that makes sense for the array_like values)
    #   -> Disallow nested structures as vars' values? ... and check that the
    #      aforementioned conditions are satisfied?
    def log_vars(self, level_method, msg, **vars):
        """Log `msg` using `level_method` followed by the `vars` dict
        encoded to a JSON formatted `str`.

        E.g.
        >>> log_vars(logger.info, "message42", i=11, f=22.5, b=True, d={"k": "v"})
        logs the following encoded message at INFO:
        ':::JSON::: message42: {"i": 11, "f": 22.5, "b": true, "d": {"k": "v"}}'

        :raise: TypeError if any `vars` value is not JSON serializable
        :raise: ValueError if any ``vars`` value contains a circular reference
        NOTE The output format of this method may be easily processed by
          programs that can decode JSON serialized data structures:
          e.g. Filebeat and Logstash can do so
        """
        level_method(":::JSON::: %s: %s", msg, json.dumps(vars))

    # XXX '<XLogger root (INFO)>' ==
    #     repr(xlogging.getLogger(None)) == repr(xlogging.getLogger("root"))
    #   -> the first wraps the logging.root logging.RootLogger, the 2nd wraps
    #      the logging.Logger logging.getLogger("root") but both have the
    #      very same repr
    # TODO Do we need XLogger~s with the logging.root RootLogger?
    #   -> only accept non-empty strings as name (no false values at all)?
    def __repr__(self):
        """Return a repr just like logging.Logger."""
        # e.g. "<XLogger __main__ (INFO)>", on subclasses with subclass name
        return str(self._logger).replace("<RootLogger ", "<Logger ").replace(
            "<Logger ", f"<{self.__class__.__name__} ")


def create_logger_factory(x_logger_class: Type[XLogger]) \
        -> Callable[[str], XLogger]:
    """Return a factory method for the specified XLogger (sub)class,
    which caches all new instances that it creates.
    """
    # Cache for XLogger instances: never instantiate XLogger directly
    _logger_by_name: dict = {}

    def get_logger(name: str) -> XLogger:
        """Return an XLogger with the specified name or, if name is None,
        return an XLogger which is the root logger of the hierarchy.

        All calls with a given name return the same (cached) logger instance.
        """
        # Be stricter than logging.getLogger(), which returns the root
        # logger for any false value: e.g. also False, 0, "", [], ()
        if not name and name is not None:
            raise TypeError("A logger name must be a non-empty string")
        # Or behave like logging.getLogger()? -> accept all false values
        # and cache a single root logger instance for all of them:
        # name = name if name else None
        cached_logger = _logger_by_name.get(name)
        return cached_logger if cached_logger \
            else _logger_by_name.setdefault(name, x_logger_class(name))

    return get_logger


# TODO Idea for XLogger that doesn't allow root logger wrapping;
#   could also be used to set loglevel from outside
#: def set_logger_level(self, level, name=None):
#:     """Set threshold for named logger, else for the root logger to `level`.
#:     """
#:     logging.getLogger(name).setLevel(level)


getLogger = create_logger_factory(XLogger)


# For interactive tests
if __name__ == "__main__":
    logger = getLogger(__name__)
