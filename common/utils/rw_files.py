import json
from pathlib import Path
import os
import base64
import zlib
from datetime import datetime
import shutil
import time
import pytz
import yaml
from common.utils import xlogging
from common.utils import file_handling_exceptions

CONFIG_DATA_PATH = (
    Path(__file__).resolve().parents[2]
    / "config"
    / "machine-connector-config.json"
)

LOGGER = xlogging.getLogger(__name__)


def load_yaml_or_json(path: Path):
    """Helper function for loading yaml or json files.

    This function loads an yaml or json file using the
    given path and returns a suitable data structure (dict).

    Args:
        path (Path): Path of the json/yaml file.

    Returns:
        dict containing the contents of the yaml or json file.

    """
    try:
        path = Path(path)
        if path.suffix in ['.yaml', '.yml']:
            with open(path) as f:
                yaml_file = yaml.load(f, Loader=yaml.FullLoader)
            return add_paths_to_config(yaml_file)
        elif path.suffix == '.json':
            with open(path) as f:
                json_file = json.load(f)
            return add_paths_to_config(json_file)
        else:
            raise ValueError('Not a json or yaml file.')
    except Exception as e:
        raise ValueError(f'Error in Loading Config File. \n {e}')

def add_paths_to_config(config_file):
    """_summary_ appending success_path,error_conversion_path,error_connection_path to config file

    Args:
        config_file (_type_): _description_= config file to append the additional Data

    Returns:
        _type_: _description_ return the config files
    """
    if config_file.get('reader') is not None:
        storage_base_path = config_file["reader"]["storage_base_path"]
        if config_file.get('reader').get('endpoints') is not None:
            for endpoint in config_file['reader']['endpoints']:
                if endpoint.get("reader_folder_path") is not None:
                    endpoint["reader_folder_path"] = storage_base_path + "\\" + endpoint["reader_folder_path"]
                    endpoint["reader_success_path"] = endpoint["reader_folder_path"] + "\\success"
                    endpoint["reader_error_conversion_path"] = endpoint["reader_folder_path"] + "\\error_conversion"
                    endpoint["reader_error_connection_path"] = endpoint["reader_folder_path"] + "\\error_connection"

    if config_file.get('writer') is not None:
        storage_base_path = config_file["writer"]["storage_base_path"]
        if config_file.get('writer').get('endpoints') is not None:
            for endpoint in config_file['writer']['endpoints']:
                if endpoint.get("writer_folder_path") is not None:
                    endpoint["reader_success_path"] = config_file["reader"]["storage_base_path"] + "\\" + endpoint["machine_vendor_type"] + "\\success"
                    endpoint["writer_folder_path"] = storage_base_path + "\\" + endpoint["writer_folder_path"]
                    endpoint["writer_original_ml_result"] = endpoint["writer_folder_path"] + "\\original_ml_result"
                    endpoint["writer_merged_ml_result"] = endpoint["writer_folder_path"] + "\\merged_ml_result"
                    endpoint["writer_error_timeout_path"] = endpoint["writer_folder_path"] + "\\error_timeout_path"
                    endpoint["writer_error_conversion_path"] = endpoint["writer_folder_path"] + "\\error_conversion_path"

    return config_file

def file_forwarding(source_path, move_to_path, file_name):
    """File Move"""
    try:
    # Moving the file after processing
        shutil.move(source_path, move_to_path + '\\' + file_name)
        modification_time = time.time()
        access_time = os.stat(move_to_path + '/' + file_name).st_atime
        os.utime(move_to_path + '/' + file_name, (access_time, modification_time))
        LOGGER.info(f"- Logfile {file_name} moved to {move_to_path}")
    except OSError as e:
        raise file_handling_exceptions.FileForwardingOsErrorException(file_name, move_to_path, e)



def compress_in_deflate(uncompressed_content: str) -> str:
    """Compress incoming content in deflate.

    The content String will be 'utf-8' encoded and deflate compressed.
    After the compression the strin will be base64 encoded.

    Args:
        uncompressed_content: Input for compression.

    Returns:
        a compressed String
    """
    bytes_test = uncompressed_content.encode("utf-8")
    deflate_compress = zlib.compressobj(9, zlib.DEFLATED, -zlib.MAX_WBITS)
    compressed_content = (
        deflate_compress.compress(bytes_test) + deflate_compress.flush()
    )
    return base64.b64encode(compressed_content).decode("ascii")

# defult time zone is "UTC"  else the time stamp will be provided based on the configured time zone
def format_time_utc_iso8601(timestamp, device_timezone="Etc/UTC"):
    """Convert time to iso."""
    device_timezone = pytz.timezone(device_timezone)
    return datetime.fromtimestamp(timestamp, device_timezone).strftime("%Y-%m-%dT%H:%M:%SZ")

def input_quit(message: str):
    """Quit application"""
    # Function is called when ever a mandatory parameter or process is missing/errornous
    LOGGER.error(message)
    LOGGER.error("- Application will be shut down!")
    # sys.exit()

def read_data(path: Path):
    """Helper function for loading csv files.
        Args:
            path (Path): Path of the yaml/json file.
        Returns:
            string containing the contents of the csv file.
        """
    LOGGER.info(path)
    if path.suffix.lower() in ['.csv', '.xml']:
        with open(path) as f:
            return f.read()
    else:
        raise ValueError('Not a .csv, .xml file.')
