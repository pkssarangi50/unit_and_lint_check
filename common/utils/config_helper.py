from typing import List

from common.utils import rw_files

# Defining custom config class, this should be adopted based on our config
class Config:
    def __init__(
            self,
            ie_databus_host: str,
            ie_databus_port: str,
            ie_databus_username: str,
            ie_databus_password: str,
            startup_wait_time_in_sec: int,
            cycle_time_in_sec: int,
            max_file_size_limit: int,
            reader: "ReaderConfig" = None,
            writer: "WriterConfig" = None
    ):
        self.ie_databus_host = ie_databus_host
        self.ie_databus_port = ie_databus_port
        self.ie_databus_username = ie_databus_username
        self.ie_databus_password = ie_databus_password
        self.startup_wait_time_in_sec = startup_wait_time_in_sec
        self.cycle_time_in_sec = cycle_time_in_sec
        self.max_file_size_limit = max_file_size_limit
        self.reader = reader
        self.writer = writer

# Defining the reader configuration class
class ReaderConfig:
    def __init__(
            self,
            storage_is_mount: bool,
            storage_mount_username: str,
            storage_mount_password: str,
            storage_base_path: str,
            endpoints: List["ReaderEndpoint"],
            component: "ComponentConfig"
    ):
        self.storage_is_mount = storage_is_mount
        self.storage_mount_username = storage_mount_username
        self.storage_mount_password = storage_mount_password
        self.storage_base_path = storage_base_path
        self.endpoints = endpoints
        self.component = component

# Defining the writer configuration class
class WriterConfig:
    def __init__(
            self,
            storage_is_mount: bool,
            storage_mount_username: str,
            storage_mount_password: str,
            storage_base_path: str,
            max_ml_result_timeout_in_sec: int,
            endpoints: List["WriterEndpoint"],
            component: "ComponentConfig"
    ):
        self.storage_is_mount = storage_is_mount
        self.storage_mount_username = storage_mount_username
        self.storage_mount_password = storage_mount_password
        self.storage_base_path = storage_base_path
        self.max_ml_result_timeout_in_sec = max_ml_result_timeout_in_sec
        self.endpoints = endpoints
        self.component = component

# Defining the endpoint class for reader configuration
class ReaderEndpoint:
    def __init__(
            self,
            active: bool,
            file_extension: List[str],
            machine_station_name: str,
            machine_vendor_type: str,
            machine_type: str,
            file_content_time_zone: str,
            interface_version: str,
            out_topic: str,
            compression: bool,
            reader_folder_path: str,
            reader_success_path: str,
            reader_error_conversion_path: str,
            reader_error_connection_path: str
    ):
        self.active = active
        self.file_extension = file_extension
        self.machine_station_name = machine_station_name
        self.machine_vendor_type = machine_vendor_type
        self.machine_type = machine_type
        self.file_content_time_zone = file_content_time_zone
        self.interface_version = interface_version
        self.out_topic = out_topic
        self.compression = compression
        self.reader_folder_path = reader_folder_path
        self.reader_success_path = reader_success_path
        self.reader_error_conversion_path = reader_error_conversion_path
        self.reader_error_connection_path = reader_error_connection_path

# Defining the endpoint class for writer configuration
class WriterEndpoint:
    def __init__(
            self,
            active: bool,
            file_extension: List[str],
            ml_in_topic: str,
            machine_station_name: str,
            machine_vendor_type: str,
            machine_type: str,
            content_time_zone: str,
            interface_version: str,
            writer_folder_path: str,
            reader_success_path: str,
            writer_original_ml_result: str,
            writer_merged_ml_result: str,
            writer_error_timeout_path: str,
            writer_error_conversion_path: str            
    ):
        self.active = active
        self.file_extension = file_extension
        self.ml_in_topic = ml_in_topic
        self.machine_station_name = machine_station_name
        self.machine_vendor_type = machine_vendor_type
        self.machine_type = machine_type
        self.content_time_zone = content_time_zone
        self.interface_version = interface_version
        self.writer_folder_path = writer_folder_path
        self.reader_success_path = reader_success_path
        self.writer_original_ml_result = writer_original_ml_result
        self.writer_merged_ml_result = writer_merged_ml_result
        self.writer_error_timeout_path = writer_error_timeout_path
        self.writer_error_conversion_path = writer_error_conversion_path

# Defining the component configuration class
class ComponentConfig:
    def __init__(
            self,
            component_type: int,
            component_version: str,
            heartbeat_time_in_sec: int,
            component_id: str,
            heart_beat_time_zone: str,
            heartbeat_topic: str
    ):
        self.component_type = component_type
        self.component_version = component_version
        self.heartbeat_time_in_sec = heartbeat_time_in_sec
        self.component_id = component_id
        self.heart_beat_time_zone = heart_beat_time_zone
        self.heartbeat_topic = heartbeat_topic

class ConfigLoader:
    """Loading the Config Object """
    def __init__(self, config_file_path):
        self.config_file_path = config_file_path
        self.config = None

    def load_config(self) -> None:
        try:
            # Load the configuration from the YAML or JSON file
            self.config = rw_files.load_yaml_or_json(self.config_file_path)
        except Exception as e:
            print(f"Error loading config from file: {str(e)}")

    def _validate_fields(self, config_object, required_fields):
        for field, expected_type in required_fields:
            if field not in config_object:
                print(f"Missing required field in config: {field}")
                return False
            if not isinstance(config_object[field], expected_type):
                print(f"Invalid data type for field '{field}'. Expected {expected_type.__name__}, got {type(config_object[field]).__name__}")
                return False

        return True

    def validate_config(self) -> bool:
        if self.config is None:
            print("No config loaded.")
            return False

        required_fields = [
            ("ie_databus_host", str),
            ("ie_databus_port", str),
            ("ie_databus_username", str),
            ("ie_databus_password", str),
            ("startup_wait_time_in_sec", int),
            ("cycle_time_in_sec", int),
            ("max_file_size_limit", int),
        ]

        if not self._validate_fields(self.config, required_fields):
            return False

        # Validate the reader configuration
        if "reader" in self.config and self.config.get("reader").get("endpoints") is not None:
            if "reader" in self.config and not self._validate_reader_config(self.config["reader"]):
                return False

        # Validate the writer configuration
        if "writer" in self.config and not self._validate_writer_config(self.config["writer"]) and "reader" in self.config:
            return False

        return True

    def _validate_reader_config(self, reader_config) -> bool:
        required_fields = [
            ("storage_is_mount", bool),
            ("storage_mount_username", str),
            ("storage_mount_password", str),
            ("storage_base_path", str),
            ("endpoints", list),
            ("component", dict),
        ]

        if not self._validate_fields(reader_config, required_fields):
            return False

        # Validate endpoints
        for endpoint in reader_config["endpoints"]:
            if not self._validate_reader_endpoint(endpoint):
                return False

        return True

    def _validate_reader_endpoint(self, endpoint) -> bool:
        required_fields = [
            ("active", bool),
            ("file_extension", list),
            ("machine_station_name", str),
            ("machine_vendor_type", str),
            ("machine_type", str),
            ("file_content_time_zone", str),
            ("interface_version", str),
            ("out_topic", str),
            ("compression", bool),
            ("reader_folder_path", str),
            ("reader_success_path", str),
            ("reader_error_conversion_path", str),
            ("reader_error_connection_path", str),
        ]

        return self._validate_fields(endpoint, required_fields)

    def _validate_writer_config(self, writer_config) -> bool:
        required_fields = [
            ("storage_is_mount", bool),
            ("storage_mount_username", str),
            ("storage_mount_password", str),
            ("storage_base_path", str),
            ("max_ml_result_timeout_in_sec", int),
            ("endpoints", list),
            ("component", dict),
        ]

        if not self._validate_fields(writer_config, required_fields):
            return False

        # Validate endpoints
        for endpoint in writer_config["endpoints"]:
            if not self._validate_writer_endpoint(endpoint):
                return False

        return True

    def _validate_writer_endpoint(self, endpoint) -> bool:
        required_fields = [
            ("active", bool),
            ("file_extension", list),
            ("ml_in_topic", str),
            ("machine_station_name", str),
            ("machine_vendor_type", str),
            ("machine_type", str),
            ("content_time_zone", str),
            ("interface_version", str),
            ("writer_folder_path", str),
            ("writer_original_ml_result", str),
            ("writer_merged_ml_result", str),
            ("writer_error_timeout_path", str),
            ("writer_error_conversion_path", str),
        ]

        return self._validate_fields(endpoint, required_fields)

    def _create_reader_endpoint(self, endpoint_config) -> ReaderEndpoint:
        return ReaderEndpoint(
            active=endpoint_config["active"],
            file_extension=endpoint_config["file_extension"],
            machine_station_name=endpoint_config["machine_station_name"],
            machine_vendor_type=endpoint_config["machine_vendor_type"],
            machine_type=endpoint_config["machine_type"],
            file_content_time_zone=endpoint_config["file_content_time_zone"],
            interface_version=endpoint_config["interface_version"],
            out_topic=endpoint_config["out_topic"],
            compression=endpoint_config["compression"],
            reader_folder_path=endpoint_config["reader_folder_path"],
            reader_success_path=endpoint_config["reader_success_path"],
            reader_error_conversion_path=endpoint_config["reader_error_conversion_path"],
            reader_error_connection_path=endpoint_config["reader_error_connection_path"]
        )

    def _create_writer_endpoint(self, endpoint_config) -> WriterEndpoint:
        return WriterEndpoint(
            active=endpoint_config["active"],
            file_extension=endpoint_config["file_extension"],
            ml_in_topic=endpoint_config["ml_in_topic"],
            machine_station_name=endpoint_config["machine_station_name"],
            machine_vendor_type=endpoint_config["machine_vendor_type"],
            machine_type=endpoint_config["machine_type"],
            content_time_zone=endpoint_config["content_time_zone"],
            interface_version=endpoint_config["interface_version"],
            writer_folder_path=endpoint_config["writer_folder_path"],
            reader_success_path=endpoint_config["reader_success_path"],
            writer_original_ml_result=endpoint_config["writer_original_ml_result"],
            writer_merged_ml_result=endpoint_config["writer_merged_ml_result"],
            writer_error_timeout_path=endpoint_config["writer_error_timeout_path"],
            writer_error_conversion_path=endpoint_config["writer_error_conversion_path"]
        )

    def _create_component_config(self, component_config) -> ComponentConfig:
        return ComponentConfig(
            component_type=component_config["component_type"],
            component_version=component_config["component_version"],
            heartbeat_time_in_sec=component_config["heartbeat_time_in_sec"],
            component_id=component_config["component_id"],
            heart_beat_time_zone=component_config["heart_beat_time_zone"],
            heartbeat_topic=component_config["heartbeat_topic"]
        )

    def convert_to_config_object(self) -> Config:
        # Convert the loaded and validated config to a custom config object
        if not self.validate_config():
            return None # type: ignore
        config_object = Config(
            ie_databus_host=self.config["ie_databus_host"],
            ie_databus_port=self.config["ie_databus_port"],
            ie_databus_username=self.config["ie_databus_username"],
            ie_databus_password=self.config["ie_databus_password"],
            startup_wait_time_in_sec=self.config["startup_wait_time_in_sec"],
            cycle_time_in_sec=self.config["cycle_time_in_sec"],
            max_file_size_limit=self.config["max_file_size_limit"],
        )       

        # Reader configuration
        if "reader" in self.config:
            reader_endpoints = []
            if self.config.get('reader').get('endpoints') is not None:
                reader_endpoints = [self._create_reader_endpoint(endpoint) for endpoint in self.config["reader"]["endpoints"]]

            reader_config = ReaderConfig(
                storage_is_mount=self.config["reader"]["storage_is_mount"],
                storage_mount_username=self.config["reader"]["storage_mount_username"],
                storage_mount_password=self.config["reader"]["storage_mount_password"],
                storage_base_path=self.config["reader"]["storage_base_path"],
                endpoints=reader_endpoints,
                component=self._create_component_config(self.config["reader"]["component"])
            )
            config_object.reader = reader_config

        if "writer" in self.config:
            # Writer configuration
            writer_config = WriterConfig(
                storage_is_mount=self.config["writer"]["storage_is_mount"],
                storage_mount_username=self.config["writer"]["storage_mount_username"],
                storage_mount_password=self.config["writer"]["storage_mount_password"],
                storage_base_path=self.config["writer"]["storage_base_path"],
                max_ml_result_timeout_in_sec=self.config["writer"]["max_ml_result_timeout_in_sec"],
                endpoints=[self._create_writer_endpoint(endpoint) for endpoint in self.config["writer"]["endpoints"]],
                component=self._create_component_config(self.config["writer"]["component"])
            )
            config_object.writer = writer_config

        return config_object
