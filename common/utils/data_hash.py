"""Hashlib.

Creates hashes.
"""
import json
from hashlib import blake2b


def hash_data(input_data: dict, digest_size: int = 25) -> str:
    """ Create a aggregated hash on data and measurement_type.
    The hash is used to uniquely identify a set of processing data.

    :input_data: raw data or preprocessed data (depends on microservice) as a dictionary
    :digest_size: Size of output digest in bytes

    return: Returns the hexdigits as string with length 2x digest_size.
    """
    blake2b_hash = blake2b(digest_size=digest_size)
    blake2b_hash.update(json.dumps(input_data).encode())
    return blake2b_hash.hexdigest()
