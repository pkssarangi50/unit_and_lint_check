"""Machine Observer Module.
"""
import threading
from common.utils.config_helper import Config, ReaderEndpoint, WriterEndpoint
from common.utils import xlogging
from common.mqtt.mac_mqtt import MacMqtt
from apps.file_reader import file_reader
from apps.file_writer import file_writer
from apps.heartbeat.heartbeat import Heartbeat


class ConnectorsExecutor():
    """The main class of this application."""
    def __init__(self, config:Config , mqtt_client: MacMqtt):
        self.logger = xlogging.getLogger(
            self.__module__ + "." + self.__class__.__name__)
        self.app_config = config
        self.mqtt_client = mqtt_client

    def fileReader_executor(self):
        """Block the current thread and run the MQTT event loop forever."""
        # Initial wait timer to ensure mqtt is connected
        self.logger.info(f"Reader initialization started")
        reader_heartbeat = Heartbeat(component_config=self.app_config.reader.component,mqtt_client=self.mqtt_client)
        reader_heartbeat_thread = threading.Thread(target=reader_heartbeat.start_heartbeat, name="reader_heartbeat_thread")
        reader_heartbeat_thread.start()
        for endpoint in self.app_config.reader.endpoints:
            endpoint : ReaderEndpoint = endpoint
            if not endpoint.active:
                continue
            try:
                file_reader_ref = file_reader.FileReader(app_config=self.app_config, mqtt_client=self.mqtt_client, endpoint=endpoint, heartbeat=reader_heartbeat)
                file_reader_ref.start_reader()
            except Exception as e:
                self.logger.error(f"File Reader for - {endpoint.machine_station_name} - directory connect failed")
        self.logger.info(f"Reader initialization completed")

    def fileWriter_executor(self):
        """Block the current thread and run the MQTT event loop forever."""
        # Initial wait timer to ensure mqtt is connected
        self.logger.info(f"Writer initialization started")
        writer_heartbeat = Heartbeat(component_config=self.app_config.writer.component,mqtt_client=self.mqtt_client)
        writer_heartbeat_thread = threading.Thread(target=writer_heartbeat.start_heartbeat, name="writer_heartbeat_thread")
        writer_heartbeat_thread.start()
        for endpoint in self.app_config.writer.endpoints:
            endpoint : WriterEndpoint = endpoint
            if not endpoint.active:
                continue
            try:
                file_writer_ref = file_writer.FileWriter(app_config=self.app_config, mqtt_client=self.mqtt_client, endpoint=endpoint, heartbeat=writer_heartbeat)
                writer_subscribe_thread = threading.Thread(target=file_writer_ref.subscribe_ml_topic, name=f"writer_subscribe_thread_{endpoint.machine_vendor_type}")
                writer_start_thread = threading.Thread(target=file_writer_ref.start_writer, name=f"writer_start_thread_{endpoint.machine_vendor_type}")
                
                writer_start_thread.start()
                writer_subscribe_thread.start()
            except Exception as e:
                self.logger.error(f"File Writer for - {endpoint.machine_station_name} - directory connect failed")
        self.logger.info(f"Writer initialization completed")
                


