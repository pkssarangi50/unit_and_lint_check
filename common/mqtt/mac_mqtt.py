"""MQTT communication adapter

This module wraps the whole mqtt communication thing so that you do not see
anything of it.
Just create a MacMqttConfiguration object and hand it over to your instance of
MacMqtt.
The connection will be established automatically and you will be reconnected
asap.
By subscribing to specific topics your topic-specific callback function is being
called when a message is being recevied there.
By publishing a message, your content is being sent to the named topic.
"""

import json
import logging
import random
import string
import time
import threading
import paho.mqtt.client as mqtt
from common.utils import xlogging

LOGGER = xlogging.getLogger(__name__)

class MacMqttConfiguration:
    """
    The class MacMqttConfiguration takes all the configuration
    arguments necessary to build a mqtt connection
    """

    def __init__(self, broker_address, broker_port, mqtt_username,
                 mqtt_password, client_id, microservice_name):
        self.mqtt_username = mqtt_username
        self.mqtt_password = mqtt_password
        self.broker_address = broker_address
        self.broker_port = broker_port
        self.broker_timeout = 60
        self.client_id = client_id
        self.clean_session = True
        self.microservice_name = microservice_name
        self.standard_connection_retries = 20
        self.protocol = 'MQTTv311'
        self.retry_wait = 5
        self.qos = 2
        self.retain = False

    def __del__(self):
        """Destructor"""


class MacMqtt:
    """
    The class "MQTT" provides basic functionility:
    creation of an MQTT client and connection to the specified broker,
    private connections are possible with username and password
    """

    def __init__(self, configuration, **kwargs):
        if 'logger_basename' in kwargs:
            self.logger_name = kwargs.get('logger_basename') + '.' + __name__
        else:
            self.logger_name = __name__
        self.logger = logging.getLogger(self.logger_name)
        if 'logger_basename' not in kwargs:
            self.logger.warning('You shall set the "logger_basename" for %s.',
                                __name__)

        self.config = configuration
        self.topic_functions = dict()
        local_id = self.config.client_id + \
            time.strftime('%H:%M:%S') + _random_string(10)
        self.client = mqtt.Client(local_id, self.config.clean_session)
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_subscribe = self.on_subscribe
        self.client.on_message = self.on_message
        self.client.on_publish = self.on_publish
        self.connection_state = False
        self.retry_counter = 0

        """
        This function builds an MQTT connection.
        In case the broker is not availble, it blocks
        the app until the connection is established.
        """
        self.connect_mqtt()

    def __del__(self):
        """
        Destructor
        By disposal of instance the send/receive loop is stopped
        and the connection is terminated.
        """
        self.client.disconnect()
        self.client.loop_stop()

    def connect_mqtt(self):
        """
        Connects Mqtt client with the given parameters, this function
        belongs (thus is executed) to the class _init_
        """
        try:
            if self.config.mqtt_username and self.config.mqtt_password:
                self.logger.info('Session with authentication started.')
                username = self.config.mqtt_username
                password = self.config.mqtt_password
                self.client.username_pw_set(username, password)
                self.logger.info('Session with authentication completed.')
            else:
                self.logger.info('Session without authentication started.')
        except Exception as err:
            self.logger.error('Could not retrieve MQTT secrets, %s', str(err))
            raise

        # Endless loop when MQTT Broker is not available.
        while self.connection_state is False:
            self.retry_counter += 1
            try:
                self.logger.info('Connecting to broker %s at port %s...',
                                 self.config.broker_address,
                                 str(self.config.broker_port))
                self.client.connect(self.config.broker_address,
                                    self.config.broker_port,
                                    self.config.broker_timeout)
                self.client.loop_start()
            except (ConnectionRefusedError, ConnectionError,
                     ConnectionAbortedError) as err:  # pylint disable=broad-except
                if (self.retry_counter >=
                        self.config.standard_connection_retries):
                    self.logger.error('Maximum connection attempts reached, '
                                      'Failed to create MQTT broker '
                                      'connection: %s', str(err))
                else:
                    self.logger.warning('Failed to create MQTT broker '
                                        'connection: %s', str(err))
            time.sleep(self.config.retry_wait)

    def publish(self, message, topic, service_qos=None,
                service_retain=None):
        """
        Publishes message to mqtt broker at the given topic
        """
        self.logger.info('Message publish started')
        if service_qos is None:
            service_qos = self.config.qos
        if service_retain is None:
            service_retain = self.config.retain
        message_payload = self.convert_message_to_json(message)
        self.logger.debug('Publishing message to %s...', topic)
        pub_result = self.client.publish(topic, message_payload, qos=service_qos,
                                         retain=service_retain)
        if pub_result[0] == 0:
            self.logger.debug('Message published to %s, message ID: %s, '
                              'payload: %s', topic, str(pub_result[1]), message_payload)
        else:
            self.logger.error('Error publishing to topic %s, message ID: %s, '
                              'publish result code: %s, payload: %s', topic,
                              str(pub_result[1]), str(pub_result[0]), message_payload)
        

    def convert_message_to_json(self, message):
        '''
        In order to allow MQTT to publish other payload type, the try/except clause is
        implemented here to convert the payload into a JSON-String. If not possible, then send
        the payload directly as it is. If the payload is not a string, bytearray, int, float or
        None, an error will be raised. This will crash the app.
        '''
        self.logger.info('Convert message to json started')
        try:
            message_payload = json.dumps(message)
        except TypeError:
            message_payload = message
        self.logger.info('Convert message to json completed')
        return message_payload
    
    def start_communication_and_loop_forever(
            self, subscriptions,
            broker_address: str = 'dsa-communication',
            broker_port: int = 9883,
            broker_username=None,
            broker_password=None):
        """Start MQTT communication and loop forever.

        Connect to the MQTT broker, subscribe the specified and some default
        subscriptions, publish the specified messages (if any),
        then run the MQTT event loop in the current thread (blocks forever).

        Automatically subscribes "config/logging" to self._handle_loglevel
        (only specify it for overriding with a different handler).

        :subscriptions: dict with MQTT topics as keys and callbacks as values
        :broker_address: the broker address to which the MQTT client should
            connect to. Default is 'dsa-communication'.
        :broker_port: the port number via which the MQTT client should connect
            to the MQTT broker. Default is 1883.
        """
        subscriptions.setdefault(
            f"{self.config.microservice_name}/config/logging", self._handle_loglevel)
        subscriptions.setdefault(
            f"{self.config.microservice_name}/config/in", self._handle_configuration)

        self.logger.info(
            "Starting connection and subscriptions to %s...", broker_address)
        self.logger.debug("Topics to subscribe: %s",
                          list(subscriptions.keys()))
        self.connect_and_subscribe(subscriptions)
        self.logger.info("Connected and subscribed to %s.", broker_address)

        # TODO Should also request the logging config, not only the
        # Blocks and runs the MQTT event loop forever
        self.logger.info("Running the MQTT event loop forever in thread: %s...",
                         threading.current_thread())
        self.loop_forever()
        mqtt
    
    def _handle_loglevel(self, msg_payload):
        """Update log level of self.logger."""
        try:
            logger_name = msg_payload.get('logger_name')
            loglevel = msg_payload['loglevel']
        except KeyError:
            LOGGER.error("No key 'loglevel' in msg_payload.")
            return
        try:
            LOGGER.info('Setting log level to %s...', loglevel)
            xlogging.getLogger(logger_name).setLevel(loglevel)
        except (TypeError, ValueError):
            LOGGER.error("Invalid loglevel: %s", loglevel)
            
    def _handle_configuration(self, msg_payload):
        """Default configuration handler.

        Updates self.config and call self.configure.

        :param msg_payload:
        """
        self.logger.info("Processing configurations received...")
        self.logger.debug(f"Configurations received: {msg_payload}")

    def _configure(self, configured_keys):
        """Function executed every time new configuration arrives.

        :param configured_keys: list of keys that contain configuration. New configuration can be
            then found in self.config object.
        """
        raise NotImplementedError()
    
    def connect_and_subscribe(self, subscriptions):
        # TODO Replace mac_mqtt with own _MqttClient
        if subscriptions:
            self.logger.info(
                "Starting initial topic subscriptions upon connection...")
            for topic, callback in subscriptions.items():
                self.subscribe(topic=topic, callback=callback)
            self.logger.info("Initial topics subscribed.")
            

    def subscribe(self, topic, callback):
        """
        Subscribes to mqtt broker at the given topic
        """
        if topic in self.topic_functions:
            self.logger.warning('Topic %s already registered. Callback '
                                'function will be updated.', topic)
        self.topic_functions.update({topic: callback})
        if self.connection_state is True:
            self.logger.debug('Subscribing to topic %s...', topic)
            result = self.client.subscribe(topic, self.config.qos)
            if result[0] == 0:
                self.logger.debug('Subscribed to topic %s, subscription ID: %s...',
                                  topic, str(result[1]))
            else:
                self.logger.error('Error subscribing to topic %s, '
                                  'subscription ID: %s, subscription result code: %s',
                                  topic, str(result[1]), str(result[0]))
        else:
            self.logger.warning(
                'Broker not connected. Subcription will be handled at on_connect.')

    def on_connect(self, client, userdata, flags, response_code):
        """
        The callback for when the client receives a CONNACK
        response from the server
        """
        del client, userdata, flags  # not used in this callback function

        if response_code == 0:
            self.connection_state = True
            self.logger.info('%s connected to %s successfully.',
                             self.config.microservice_name, self.config.broker_address)
            if self.topic_functions:
                self.logger.info(
                    "Starting initial topic subscriptions upon connection...")
                for key in self.topic_functions:
                    result = self.client.subscribe(key, self.config.qos)
                    if result[0] == 0:
                        self.logger.debug('Subscribed to topic %s,'
                                          'subscription ID: %s...',
                                          key, str(result[1]))
                    else:
                        self.logger.error('Error subscribing to topic %s, '
                                          'subcription ID: %s, subscription result code: %s',
                                          key, str(result[1]), str(result[0]))

                self.logger.info("Initial topics subscribed.")
        else:
            self.connection_state = False
            self.logger.error('%s failed to connect to %s, result code: %s',
                              self.config.microservice_name, self.config.broker_address,
                              str(response_code))

    def on_message(self, client, userdata, msg):
        """
        The callback for when a message is received from the server
        at the subscribed topic
        """
        del client, userdata  # not used in this callback function

        self.logger.debug('Message received by %s, topic: %s, payload: %s',
                          self.config.microservice_name, msg.topic, msg.payload)
        # Until base-libs==0.7.3, it is assumed that the payload must be JSON or JSON
        # serializable. With paho-mqtt==1.5.0, exceptions are automatically suppressed.
        # So if json.loads fails, no exception will be thrown and no error
        # message will be shown. With paho-mqtt==1.5.1, exceptions are not
        # swallowed by default, and the first observation is that, if the incoming
        # message is not a JSON and json.load is executed, the app will not crash, although
        # we assume that app shall terminate due to unhandled exception. Instead, the app gets
        # stuck and cannot process further incoming message. First assumption is that the
        # thread died (?).
        # Furthermore, there are requirements in other projects which needs to use MQTT to
        # transfer data which are not JSON serializable (for e.g. a file binary). The
        # ability of MQTT to transfer other data types shall not be limited to JSON only.
        # TODO: We will first stick to paho-mqtt==1.5.0 first. By implementing
        # try/except here, we will try to catch the exception if json.loads is not successful.
        # We will then pass the msg.payload directly through since in some usecases, other data
        # format than JSON is required. The try/except clause is to ensure that the
        # implementations in older projects, which rely on JSON payload, won't break.
        # The compromise is that, even if the exception wrapper in the project implementation
        # is deactivated, the app won't crash as expected.

        # A long term solution should be implemented, and breaking changes (which pass payload
        # directly through, JSON loading should be implemented in project) shall be expected
        # in the near future!
        new_message = self.convert_message_to_json(msg.payload)
            # self.logger.error("Error in parsing received JSON message - topic: "
            #                   "%s, payload: %s. Traceback: ",
            #                   msg.topic, msg.payload, exc_info=True)

        # try/except block is not needed here since any exceptions in the callback function will be
        # caught by _base_wrapper, which is activated per default (and cannot be deactivated)
        # Using paho-mqtt==1.5.0, if exception_wrapper is not activated. exceptions
        # will be raised and paho-mqtt will just swallow this exception, and
        # the app won't crash as expected.
        self.topic_functions[msg.topic](new_message)

    def on_subscribe(self, client, userdata, mid, granted_ops):
        """
        Called when the broker responds to a subscribe request
        """
        del client, userdata, granted_ops  # not used in this callback function

        self.logger.debug(
            'Subscription acknowledged, subscription ID: %s', str(mid))

    def on_publish(self, client, userdata, mid):
        """
        Called when the message sending to
        the broker has been completed
        """
        del client, userdata  # not used in this callback function

        self.logger.debug(
            'Published message acknowledged, message ID: %s', str(mid))

    def on_disconnect(self, client, userdata, response_code):
        """
        Executed when the connection to the broker has been interrupted,
        either from the server or client side
        """
        del client, userdata  # not used in this callback function

        self.connection_state = False
        if response_code == 0:
            self.logger.info('Connection between %s and %s terminated by client, response code: %s',
                             self.config.microservice_name, self.config.broker_address,
                             str(response_code))
        else:
            self.logger.warning('Connection between %s and %s ended unexpectedly from server, '
                                'response code: %s. Attempting reconnection...',
                                self.config.microservice_name, self.config.broker_address,
                                str(response_code))
            self.client.reconnect()

    def is_connected(self):
        """MQTT Broker is connected"""
        return self.connection_state
    
    def loop_forever(self):
        """Block the current thread and run the MQTT event loop forever."""
        while True:
            time.sleep(0.5)

def _random_string(random_string):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=random_string))

